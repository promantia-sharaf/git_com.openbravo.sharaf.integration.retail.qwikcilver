/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

/**
 * @author ebe
 * 
 */
@ApplicationScoped
@ComponentProvider.Qualifier(CUSTQCComponentProvider.QUALIFIER)
public class CUSTQCComponentProvider extends BaseComponentProvider {

  public static final String QUALIFIER = "CUSTQC_Main";
  public static final String MODULE_JAVA_PACKAGE = "com.openbravo.sharaf.integration.retail.qwikcilver";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {

    final GlobalResourcesHelper grhelper = new GlobalResourcesHelper();

    grhelper.add("components/connectorPaymentProvider.js");
    grhelper.add("components/menu.js");
    grhelper.add("components/modalQwikCilverGiftCard.js");
    grhelper.add("components/modalQwikCilverGiftCardPin.js");
    grhelper.add("components/modalQwikCilverGiftCardTransfer.js");
    grhelper.add("components/checkOverpaymentExtension.js");
    grhelper.add("components/connectorPromoPaymentProvider.js");
    grhelper.add("components/connectorStorePromoPaymentProvider.js");
    grhelper.add("data/productProperties.js");
    grhelper.add("hooks/OBPOS_PreAddProductToOrder.js");
    //grhelper.add("hooks/OBPOS_PreDeleteCurrentOrder.js");
    grhelper.add("hooks/OBPOS_PreDeleteLine.js");
    grhelper.add("hooks/OBPOS_RenderOrderLine.js");
    //grhelper.add("hooks/OBPOS_PrePaymentHook.js");
    grhelper.add("hooks/OBPOS_preRemovePaymentHook.js");
    grhelper.add("hooks/OBRETUR_ReturnReceiptApplyHook.js");
    grhelper.add("hooks/OBPOS_PrePaymentHook_forGCPromo.js");
    grhelper.add("hooks/OBPOS_TerminalLoadedFromBackendHook.js");
    grhelper.add("models/qwikcilverModel.js");

    grhelper.add("utils.js");

    grhelper.addStyle("custqc.css");

    return grhelper.getGlobalResources();
  }

  private class GlobalResourcesHelper {
    private final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    private final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";
    private final String cssPrefix = "web/" + MODULE_JAVA_PACKAGE + "/css/";

    public void add(String file) {
      globalResources.add(
          createComponentResource(ComponentResourceType.Static, prefix + file, POSUtils.APP_NAME));
    }

    public void addStyle(String file) {
      globalResources.add(createComponentResource(ComponentResourceType.Stylesheet,
          cssPrefix + file, POSUtils.APP_NAME));
    }

    public List<ComponentResource> getGlobalResources() {
      return globalResources;
    }
  }
}
