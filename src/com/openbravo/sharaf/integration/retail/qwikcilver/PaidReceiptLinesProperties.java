/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.retail.qwikcilver;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.PaidReceipts;

/**
 * Defines hql properties for the PaidReceipsLines Order header
 * 
 * @author ebe
 * 
 */

@Qualifier(PaidReceipts.paidReceiptsLinesPropertyExtension)
public class PaidReceiptLinesProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>() {
      private static final long serialVersionUID = 1L;
      {
        add(new HQLProperty("ordLine.custqcCardnumber", "custqcCardnumber"));
        add(new HQLProperty("ordLine.custqcPurchaseamount", "custqcPurchaseamount"));
        add(new HQLProperty("ordLine.custqcApprovalcode", "custqcApprovalcode"));
        add(new HQLProperty("ordLine.custqcBatchnumber", "custqcBatchnumber"));
        add(new HQLProperty("ordLine.custqcReferenceid", "custqcReferenceId"));
        add(new HQLProperty("ordLine.custqcCardexpiry", "custqcCardexpiry"));
      }
    };

    return list;
  }
}
