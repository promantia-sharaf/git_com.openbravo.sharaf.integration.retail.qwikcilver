/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.retail.posterminal.JSONProcessSimple;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.service.json.JsonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;

public abstract class WSQwikCilverBase extends JSONProcessSimple {
  private static final Logger log = LoggerFactory.getLogger(WSQwikCilverBase.class);

  abstract protected WebServiceURL getWebServiceURL(JSONObject jsonData);

  abstract protected void callQwikCilverServer(JSONObject jsonData, JSONObject data,
      WebServiceURL wsUrl, OBPOSApplications pos);

  protected void processQwickCilverException(JSONObject result, JSONObject jsonData,
      OBPOSApplications pos) {
    throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrReadResponse", null));
  }

  @Override
  public JSONObject exec(JSONObject jsonData) {
    try {
      log.debug("Start QwikCilver request: {}", jsonData.toString(2));
    } catch (JSONException ignore) {
    }
    JSONObject response = new JSONObject();
    JSONObject data = new JSONObject();
    OBContext.setAdminMode(true);
    OBPOSApplications pos = null;
    try {
      response.put("data", data);
      response.put("status", JsonConstants.RPCREQUEST_STATUS_SUCCESS);
      WebServiceURL wsUrl = getWebServiceURL(jsonData);
      pos = OBDal.getInstance().get(OBPOSApplications.class, jsonData.optString("pos"));
      if (pos != null) {
        if (Utils.isQwikCilverInitialized(pos)) {
          callQwikCilverServer(jsonData, data, wsUrl, pos);
        } else {
          throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrNotInitialized",
              new String[] { pos.getIdentifier() }));
        }
      } else {
        throw new OBException(
            OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrNotFoundApp", null));
      }
    } catch (QwickCilverException e) {
      processQwickCilverException(response, jsonData, pos);
    } catch (JSONException ignore) {
    } finally {
      OBContext.restorePreviousMode();
    }
    JSONObject result = new JSONObject();
    try {
      result.put(JsonConstants.RESPONSE_DATA, response);
      result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (JSONException ignoree) {
    }
    return result;
  }

}
