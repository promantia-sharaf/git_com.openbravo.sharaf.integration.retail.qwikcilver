/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcPendingOp;
import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcWebserviceConfig;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetCancelActivate;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetCancelReload;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.ResultActivate;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;
import org.apache.log4j.Logger;

//It's no longer in use after OBDEV-182 Changes

public class WSCancelActivate extends WSQwikCilverBase {
  
  private static Logger log = Logger.getLogger(WSCancelActivate.class);
  
  private String getNotes(boolean isGiftcard, String cardNumber, String amount) {
    return (isGiftcard ? "Activate: " : "Reload: ") + cardNumber + " with: " + amount;
  }

  @Override
  protected WebServiceURL getWebServiceURL(JSONObject jsonData) {
    boolean isGiftcard = jsonData.optBoolean("isGiftcard");
    if (isGiftcard) {
      return Utils.getWebServiceURL(jsonData.optString("organization"),
          CustqcWebserviceConfig.PROPERTY_CANCELACTIVATEPATH,
          CustqcWebserviceConfig.PROPERTY_CANCELACTIVATEHTTPMTH,
          "CUSTQC_QwikCilverErrConfigCancelActivate");
    } else {
      return Utils.getWebServiceURL(jsonData.optString("organization"),
          CustqcWebserviceConfig.PROPERTY_CANCELRELOADPATH,
          CustqcWebserviceConfig.PROPERTY_CANCELRELOADHTTPMTH,
          "CUSTQC_QwikCilverErrConfigCancelReload");
    }
  }

  @Override
  protected void callQwikCilverServer(JSONObject jsonData, JSONObject data, WebServiceURL wsUrl,
      OBPOSApplications pos) {
    boolean isGiftcard = jsonData.optBoolean("isGiftcard");
    ResultActivate netResult;
    Map<String, String> headers = Utils.getCommonHeaders(pos);
    JSONObject body = new JSONObject();
    Double amount = jsonData.optDouble("amount");
    String cardCurrencySymbol = jsonData.optString("cardCurrencySymbol");
    String amountFormatted = Utils.getFormattedAmount(cardCurrencySymbol, amount);
    String cardNumber = Utils.getCardNumberFromDigitBarCode(jsonData.optString("cardNumber"));
    String notes = getNotes(isGiftcard, cardNumber, amountFormatted);
    String errorLog = null;
    boolean isQCPendingTxn = false;
    CustqcPendingOp pendingOp = null;
    log.info("WSCancel Activate"+jsonData.toString());    
    try {
      body.put("CardNumber", cardNumber);
      body.put("CardPIN", "");
      body.put("OriginalAmount", amountFormatted);
      body.put("OriginalApprovalCode", jsonData.getString("approvalCode"));
      body.put("OriginalBatchNumber", jsonData.getLong("batchNumber"));
      body.put("OriginalInvoiceNumber", jsonData.getString("invoiceNumber"));
      body.put("OriginalTransactionId", jsonData.getString("referenceId"));
      body.put("Notes", notes);
      String qcRequest = body.toString();
      log.info("WSCancel Activate Request"+body.toString()); 
      Long batchNumber = Long.parseLong(
          Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCCURRENTBATCHNUMBER, false));        
      pendingOp = Utils.saveQCTxn(isGiftcard ? "CANCEL_ACTIVATE" : "CANCEL_RELOAD", cardNumber, amount, "-", batchNumber,
          jsonData.getString("invoiceNumber"), notes, pos.getCustqcTransactionid(), pos, qcRequest, 
          null , null, isQCPendingTxn);
    } catch (JSONException ignore) {
    }
    if (isGiftcard) {
      NetCancelActivate cancelActivate = new NetCancelActivate();
      netResult = (ResultActivate) cancelActivate.read(wsUrl, headers, body);
    } else {
      NetCancelReload cancelReload = new NetCancelReload();
      netResult = (ResultActivate) cancelReload.read(wsUrl, headers, body);
    }
    Utils.updateTransactionId(pos, netResult);
    if (!netResult.isSuccess() && isGiftcard
        && netResult.getResponseCode().intValue() == ResponseCodes.RESPONSE_CARD_NOT_ACTIVATE) {
      try {   
        String qcResponse = netResult.getQcResponse();
        errorLog = netResult.getResponseMessage();
        Utils.saveQCResponse(pendingOp, qcResponse, errorLog);
      }catch (Exception e) {
        log.error(e);
      }    
      netResult.clearError();
    } else {
      try {   
        String qcResponse = netResult.getQcResponse();
        Utils.saveQCResponse(pendingOp, qcResponse, null);
      }catch (Exception e) {
        log.error(e);
      }  
    }
  }

  @Override
  protected void processQwickCilverException(JSONObject result, JSONObject jsonData,
      OBPOSApplications pos) {
    try {
      boolean isGiftcard = jsonData.getBoolean("isGiftcard");
      Double amount = jsonData.getDouble("amount");
      String cardCurrencySymbol = jsonData.getString("cardCurrencySymbol");
      String amountFormatted = Utils.getFormattedAmount(cardCurrencySymbol, amount);
      String cardNumber = jsonData.getString("cardNumber");
    } catch (JSONException e) {
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrReadResponse", null),
          e, true);
    }
  }

}
