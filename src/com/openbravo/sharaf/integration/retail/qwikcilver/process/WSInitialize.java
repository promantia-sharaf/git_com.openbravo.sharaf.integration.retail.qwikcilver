/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.retail.posterminal.JSONProcessSimple;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.service.json.JsonConstants;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcWebserviceConfig;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetInitialize;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.ResultInitialize;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;

public class WSInitialize extends JSONProcessSimple {

  @Override
  public JSONObject exec(JSONObject jsonData) {
    JSONObject response = new JSONObject();
    OBContext.setAdminMode(true);
    try {
      WebServiceURL wsUrl = Utils.getWebServiceURL(jsonData.optString("organization"),
          CustqcWebserviceConfig.PROPERTY_INITIALIZEREQUESTPATH,
          CustqcWebserviceConfig.PROPERTY_INITIALIZEREQUESTHTTPMTH,
          "CUSTQC_QwickCilverErrConfigInitialize");
      OBPOSApplications pos = OBDal.getInstance().get(OBPOSApplications.class,
          jsonData.optString("pos"));
      if (pos != null) {
        SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("UserName",
            Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCUSERNAME, true));
        headers.put("Password",
            Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPASSWORD, true));
        headers.put("TerminalId",
            Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCTERMINALID, true));
        headers.put("ForwardingEntityId",
            Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCFORWARDINGENTITYID, true));
        headers.put("ForwardingEntityPassword",
            Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCFORWARDINGENTITYPWD, true));
        headers.put("DateAtClient", dFormat.format(new Date()));
        headers.put("TransactionId", "1");
        NetInitialize netInitialize = new NetInitialize();
        ResultInitialize netResult = (ResultInitialize) netInitialize.read(wsUrl, headers);
        Utils.updateTransactionId(pos, netResult);
        if (netResult.isSuccess()) {
          pos.setCustqcMerchantoutletname(netResult.getMerchantOutletName());
          pos.setCustqcAcquirerid(netResult.getAcquirerId());
          pos.setCustqcOrganizationname(netResult.getOrganizationName());
          pos.setCustqcPosentrymode(netResult.getPosEntryMode());
          pos.setCustqcPostypeid(netResult.getPosTypeId());
          pos.setCustqcPosname(netResult.getPosName());
          pos.setCustqcTermappversion(netResult.getTermAppVersion());
          pos.setCustqcCurrentbatchnumber(netResult.getCurrentBatchNumber());
          pos.setCustqcDateatclient(netResult.getDateAtClient());
          OBDal.getInstance().save(pos);
          OBDal.getInstance().flush();
        } else {
          throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrEmptyPOSParam",
              new String[] { netResult.getResponseMessage() }));
        }
      } else {
        throw new OBException(
            OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrNotFoundApp", null));
      }
      response.put("status", JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (JSONException e) {
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrJson",
          new String[] { e.getMessage() }));
    } finally {
      OBContext.restorePreviousMode();
    }
    JSONObject result = new JSONObject();
    try {
      result.put(JsonConstants.RESPONSE_DATA, response);
      result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (JSONException ignoree) {
    }
    return result;
  }
}
