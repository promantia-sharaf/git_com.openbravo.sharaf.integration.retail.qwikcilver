/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.retail.posterminal.OBPOSApplications;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcWebserviceConfig;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetBalanceTransfer;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.ResultBalanceEnquiry;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;

public class WSBalanceTransfer extends WSQwikCilverBase {

  @Override
  protected WebServiceURL getWebServiceURL(JSONObject jsonData) {
    return Utils.getWebServiceURL(jsonData.optString("organization"),
        CustqcWebserviceConfig.PROPERTY_REISSUEPATH, CustqcWebserviceConfig.PROPERTY_REISSUEHTTPMTH,
        "CUSTQC_QwikCilverErrConfigBalanceTransfer");
  }

  @Override
  protected void callQwikCilverServer(JSONObject jsonData, JSONObject data, WebServiceURL wsUrl,
      OBPOSApplications pos) {
    Map<String, String> headers = Utils.getCommonHeaders(pos);
    String newTrackData = jsonData.optString("newCardNumber");
    String oldCardNumber = Utils.getCardNumberFromDigitBarCode(jsonData.optString("oldCardNumber"));
    String newCardNumber = Utils.getCardNumberFromDigitBarCode(newTrackData);
    JSONObject body = new JSONObject();
    try {
      body.put("OriginalCardNumber", oldCardNumber);
      if (newTrackData.length() > 16) {
        body.put("trackData", newTrackData);
      }
      body.put("CardNumber", newCardNumber);
      body.put("Notes", "Reissue card: " + oldCardNumber + " to: " + newCardNumber);
    } catch (JSONException ignore) {
    }
    NetBalanceTransfer balanceTranfer = new NetBalanceTransfer();
    ResultBalanceEnquiry netResult = (ResultBalanceEnquiry) balanceTranfer.read(wsUrl, headers,
        body);
    Utils.updateTransactionId(pos, netResult);
    netResult.checkErrors();
    if (netResult.isSuccess()) {
      SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
      try {
        data.put("cardNumber", newCardNumber);
        data.put("cardExpiry", dFormat.format(netResult.getCardExpiry()));
        data.put("amount", netResult.getAmount());
        data.put("currencyConversionRate", netResult.getCurrencyConversionRate());
        data.put("cardCurrencySymbol", netResult.getCardCurrencySymbol());
      } catch (JSONException ignore) {
      }
    }
  }

}
