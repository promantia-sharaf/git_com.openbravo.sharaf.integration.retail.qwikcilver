/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcPendingOp;
import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcWebserviceConfig;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetCancelRedeem;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.ResultCancelRedeem;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;
import org.apache.log4j.Logger;

public class WSCancelRedeem extends WSQwikCilverBase {
  
  private static Logger log = Logger.getLogger(WSCancelRedeem.class);

  private String getNotes(String cardNumber, String amount) {
    return "Cancel redeem of " + amount + " for: " + cardNumber;
  }

  @Override
  protected WebServiceURL getWebServiceURL(JSONObject jsonData) {
    return Utils.getWebServiceURL(jsonData.optString("organization"),
        CustqcWebserviceConfig.PROPERTY_CANCELGIFTCARDREDEEMPATH,
        CustqcWebserviceConfig.PROPERTY_CANCELGIFTCARDREDEEMHTTPMTH,
        "CUSTQC_QwikCilverErrConfigCancelRedeem");
  }

  @Override
  protected void callQwikCilverServer(JSONObject jsonData, JSONObject data, WebServiceURL wsUrl,
      OBPOSApplications pos) {
    Map<String, String> headers = Utils.getCommonHeaders(pos);
    String cardCurrencySymbol = jsonData.optString("cardCurrencySymbol");
    Double currencyConversionRate = jsonData.optDouble("currencyConversionRate");
    Double amount = jsonData.optDouble("amount");
    String amountRated = Utils.getAmountDivByRate(cardCurrencySymbol, currencyConversionRate,
        amount);
    String trackData = jsonData.optString("cardNumber");
    String cardNumber = Utils.getCardNumberFromDigitBarCode(trackData);
    String notes = getNotes(cardNumber, amountRated);
    String errorLog = null;
    boolean isQCPendingTxn = false; 
    CustqcPendingOp pendingOp = null;
    JSONObject body = new JSONObject();
    try {
      body.put("CardNumber", cardNumber);
      body.put("OriginalAmount", amountRated);
      body.put("OriginalInvoiceNumber", jsonData.optString("invoiceNumber"));
      body.put("OriginalTransactionId", jsonData.optString("referenceId"));
      body.put("OriginalBatchNumber", jsonData.optString("batchNumber"));
      body.put("OriginalApprovalCode", jsonData.optString("approvalCode"));
      body.put("Notes", notes);
      if (trackData.length() > 16) {
        body.put("trackData", trackData);
      } else {
        body.put("CardPin", jsonData.getString("cardPin"));
      }
      String qcRequest = body.toString();
      Long batchNumber = Long.parseLong(
          Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCCURRENTBATCHNUMBER, false));        
      pendingOp = Utils.saveQCTxn("CANCEL_REDEEM", cardNumber, amount, "-", batchNumber,
          jsonData.getString("invoiceNumber"), notes, pos.getCustqcTransactionid(), pos, qcRequest , 
         null , null, isQCPendingTxn);
    } catch (JSONException ignore) {
    }
    NetCancelRedeem redeem = new NetCancelRedeem();
    ResultCancelRedeem netResult = (ResultCancelRedeem) redeem.read(wsUrl, headers, body);
    Utils.updateTransactionId(pos, netResult);
    if (!netResult.isSuccess()
        && netResult.getResponseCode().intValue() == ResponseCodes.RESPONSE_ALREADY_CANCELLED) {     
      netResult.clearError();
    } else {
      netResult.checkErrors();
    }
    if (netResult.isSuccess()) {
      SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
      try {
        data.put("cardNumber", cardNumber);
        data.put("cardExpiry", dFormat.format(netResult.getCardExpiry()));
        data.put("amount", netResult.getAmount());
        data.put("redeemAmount", netResult.getRedeemAmount());
        data.put("cardCurrencySymbol", netResult.getCardCurrencySymbol());
        data.put("currencyConversionRate", netResult.getCurrencyConversionRate());
        data.put("batchNumber", netResult.getBatchNumber());
        data.put("approvalCode", netResult.getApprovalCode());
        data.put("transactionId", netResult.getTransactionId());
        data.put("notes", notes);
      } catch (JSONException ignore) {
      }
    } 
    try {   
      String qcResponse = netResult.getQcResponse();
      Utils.saveQCResponse(pendingOp, qcResponse, null);
    }catch (Exception e) {
      log.error(e);
    }   
  }

  @Override
  protected void processQwickCilverException(JSONObject result, JSONObject jsonData,
      OBPOSApplications pos) {
    try {     
      String cardCurrencySymbol = jsonData.optString("cardCurrencySymbol");
      Double currencyConversionRate = jsonData.optDouble("currencyConversionRate");
      Double amount = jsonData.optDouble("amount");
      String amountRated = Utils.getAmountDivByRate(cardCurrencySymbol, currencyConversionRate,
          amount);
      String cardNumber = jsonData.optString("cardNumber");
      String notes = getNotes(cardNumber, amountRated);
      JSONObject data = result.optJSONObject("data");
      data.put("cardNumber", cardNumber);
      data.put("amount", amount);
      data.put("redeemAmount", amountRated);
      data.put("invoiceNumber", jsonData.optString("invoiceNumber"));
      data.put("cardCurrencySymbol", cardCurrencySymbol);
      data.put("currencyConversionRate", currencyConversionRate);
      data.put("batchNumber", jsonData.optLong("batchNumber"));
      data.put("approvalCode", jsonData.optString("approvalCode"));
      data.put("referenceId", jsonData.optLong("referenceId"));
      data.put("notes", notes);
    } catch (JSONException e) {
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrReadResponse", null),
          e, true);
    }
  }
}
