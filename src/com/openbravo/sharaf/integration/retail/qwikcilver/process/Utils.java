/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.retail.posterminal.OBPOSApplications;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcPendingOp;
import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcWebserviceConfig;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceResult;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;
import com.qwikcilver.clientapi.svclient.GCWebPos;
import org.apache.log4j.Logger;

public class Utils {

  private static Logger log = Logger.getLogger(Utils.class);
  static DecimalFormatSymbols dfs = new DecimalFormatSymbols();
  static DecimalFormat df = new DecimalFormat();

  public static String getPOSValue(OBPOSApplications pos, String prop, boolean userDefined) {
    String value = (String) pos.get(prop);
    if (value == null || value.trim().equals("")) {
      throw new OBException(userDefined ? OBMessageUtils.getI18NMessage(
          "CUSTQC_QwikCilverErrEmptyPOSParam", new String[] { prop })
          : OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrInitializeServer", null));
    }
    return value;
  }

  public static boolean hasPOSValue(OBPOSApplications pos, String prop) {
    String value = (String) pos.get(prop);
    return value != null && !value.trim().equals("");
  }

  public static CustqcWebserviceConfig getWSConfig(String orgId) {
    final OBQuery<CustqcWebserviceConfig> obQuery = OBDal.getInstance().createQuery(
        CustqcWebserviceConfig.class, "organization.id = :orgId");
    obQuery.setNamedParameter("orgId", orgId);
    obQuery.setFilterOnActive(false);
    obQuery.setMaxResult(1);
    return obQuery.uniqueResult();
  }

  public static WebServiceURL getWebServiceURL(String orgId, String propPath, String propMethod,
      String msg) {
    CustqcWebserviceConfig wsConfig = Utils.getWSConfig(orgId);
    if (wsConfig == null) {
      wsConfig = Utils.getWSConfig("0");
      if (wsConfig == null) {
        throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrNotConfig", null));
      }
    }
    String wsUrl = (String) wsConfig.get(propPath);
    String wsMethod = (String) wsConfig.get(propMethod);
    if (wsUrl == null || wsUrl.trim().equals("") || wsMethod == null || wsMethod.trim().equals("")) {
      String msgError = OBMessageUtils.getI18NMessage(msg, null);
      throw new OBException(msgError == null ? "Not found message: " + msg : msgError);
    }
    return new WebServiceURL(wsUrl, wsMethod);
  }

  public static boolean isQwikCilverInitialized(OBPOSApplications pos) {
    return hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCTERMINALID)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCFORWARDINGENTITYID)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCFORWARDINGENTITYPWD)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCUSERNAME)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPASSWORD)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCDATEATCLIENT)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCMERCHANTOUTLETNAME)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCACQUIRERID)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCORGANIZATIONNAME)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPOSENTRYMODE)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPOSTYPEID)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPOSNAME)
        && hasPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCCURRENTBATCHNUMBER);
  }

  public static Map<String, String> getCommonHeaders(OBPOSApplications pos) {
    Map<String, String> headers = new HashMap<String, String>();

    headers.put("TerminalId", getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCTERMINALID, true));
    headers.put("ForwardingEntityId",
        getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCFORWARDINGENTITYID, true));
    headers.put("ForwardingEntityPassword",
        getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCFORWARDINGENTITYPWD, true));
    headers.put("UserName", getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCUSERNAME, true));
    headers.put("Password", getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPASSWORD, true));

    headers.put("DateAtClient",
        getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCDATEATCLIENT, false));
    headers.put("MerchantOutletName",
        getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCMERCHANTOUTLETNAME, false));
    headers.put("AcquirerId", getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCACQUIRERID, false));
    headers.put("OrganizationName",
        getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCORGANIZATIONNAME, false));
    headers.put("POSEntryMode",
        getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPOSENTRYMODE, false));
    headers.put("POSTypeId", getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPOSTYPEID, false));
    headers.put("POSName", getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCPOSNAME, false));
    headers.put("TermAppVersion",
        getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCTERMAPPVERSION, false));
    headers.put("CurrentBatchNumber",
        getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCCURRENTBATCHNUMBER, false));

    Long transactionId = pos.getCustqcTransactionid();
    if (transactionId == null) {
      transactionId = 1L;
    } else {
      transactionId++;
    }
    headers.put("TransactionId", transactionId.toString());
    return headers;
  }

  public static void updateTransactionId(OBPOSApplications pos, WebServiceResult result) {
    if (result.getTransactionId() != null) {
      pos.setCustqcTransactionid(result.getTransactionId());
      OBDal.getInstance().save(pos);
      OBDal.getInstance().flush();
    }
  }

  public static void checkCommonErrors(WebServiceResult result) {
    switch (result.getResponseCode().intValue()) {
    // Invalid Batch Number
    case ResponseCodes.RESPONSE_INVALID_BATCH:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10064", null));

      // Could not find card
    case ResponseCodes.RESPONSE_CARD_NOT_FOUND:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10004", null));

      // Either card number or card pin is incorrect.
    case ResponseCodes.RESPONSE_CARD_INVALID:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10086", null));
      
      // Card balance is insufficient, so unable to deduct 
    case ResponseCodes.RESPONSE_CARD_INSUFFICIENT_BALANCE:
        throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10010", null));
      
      // Card is already redeemed post activation
    case ResponseCodes.RESPONSE_CARD_ACTIVATED_AND_ALREADY_REDEEMED:
        throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10260", null));
    }
  }

  public static Currency getCurrency(String isoCode) {
    final OBQuery<Currency> obQuery = OBDal.getInstance().createQuery(Currency.class,
        Currency.PROPERTY_ISOCODE + " = :isoCode");
    obQuery.setNamedParameter("isoCode", isoCode);
    obQuery.setFilterOnActive(false);
    obQuery.setMaxResult(1);
    return obQuery.uniqueResult();
  }

  public static String getFormattedAmount(String isoCode, Double amount) {
    Currency currency = Utils.getCurrency(isoCode);
    if (currency == null) {
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrCurrencyNotFound",
          new String[] { isoCode }));
    }
    dfs.setDecimalSeparator('.');
    df.setGroupingUsed(false);
    df.setDecimalFormatSymbols(dfs);
    df.setMaximumFractionDigits(currency.getStandardPrecision().intValue());
    df.setMinimumFractionDigits(currency.getStandardPrecision().intValue());
    return df.format(amount);
  }

  public static String getAmountMultByRate(String isoCode, Double rate, Double amount) {
    Double amountRated = amount;
    if (!rate.equals(0.0)) {
      amountRated = amount * rate;
    }
    return getFormattedAmount(isoCode, amountRated);
  }

  public static String getAmountDivByRate(String isoCode, Double rate, Double amount) {
    Double amountRated = amount;
    if (!rate.equals(0.0)) {
      amountRated = amount / rate;
    }
    return getFormattedAmount(isoCode, amountRated);
  }

  public static String getCardNumberFromDigitBarCode(String barCode) {
    if (barCode.length() == 31) {
      return GCWebPos.getCardNumberFrom31DigitBarCode(barCode);
    } else if (barCode.length() == 26) {
      return GCWebPos.getCardNumberFrom26DigitBarCode(barCode);
    }
    return barCode;
  }

  public static void savePendingOp(String cardAction, String cardNumber, Double amount,
      String approvalCode, Long batchNumber, String invoiceNumber, String notes,
      Long transactionid, OBPOSApplications pos, Boolean isQCPendingTxn) {
    CustqcPendingOp pendingOp = OBProvider.getInstance().get(CustqcPendingOp.class);
    pendingOp.setQcpendingTxn(isQCPendingTxn); 
    pendingOp.setCardAction(cardAction);
    pendingOp.setCardNumber(cardNumber);
    pendingOp.setAmount(new BigDecimal(amount));
    pendingOp.setApprovalCode(approvalCode);
    pendingOp.setBatchNumber(batchNumber);
    pendingOp.setInvoiceNumber(invoiceNumber);
    pendingOp.setNotes(notes == null || notes.length() < 4000 ? notes : notes.substring(0, 4000));
    pendingOp.setTransactionid(transactionid);
    pendingOp.setProcessed(false);
    pendingOp.setPOSTerminal(pos);
    pendingOp.setUserContact(OBContext.getOBContext().getUser());
    OBDal.getInstance().save(pendingOp);
  }
  
  public static CustqcPendingOp saveQCTxn(String cardAction, String cardNumber, Double amount,
      String approvalCode, Long batchNumber, String invoiceNumber, String notes,
      Long transactionid, OBPOSApplications pos, String qcRequest, String qcResponse ,
      String errorLog, Boolean isQCPendingTxn) {
    log.info("Saving QC trans"+cardNumber); 
    CustqcPendingOp pendingOp = OBProvider.getInstance().get(CustqcPendingOp.class);
    pendingOp.setQcpendingTxn(isQCPendingTxn); 
    pendingOp.setCardAction(cardAction);
    pendingOp.setCardNumber(cardNumber);
    pendingOp.setAmount(new BigDecimal(amount));
    pendingOp.setApprovalCode(approvalCode);
    pendingOp.setBatchNumber(batchNumber);
    pendingOp.setInvoiceNumber(invoiceNumber);
    pendingOp.setNotes(notes == null || notes.length() < 4000 ? notes : notes.substring(0, 4000));
    pendingOp.setTransactionid(transactionid);
    if(errorLog == null) {    
      pendingOp.setProcessed(true);
    } else {
      pendingOp.setProcessed(false); 
    }
    pendingOp.setPOSTerminal(pos);
    pendingOp.setUserContact(OBContext.getOBContext().getUser());
    pendingOp.setRequest(qcRequest);
    pendingOp.setResponse(qcResponse);
    pendingOp.setErrorLog(errorLog);
    OBDal.getInstance().save(pendingOp);
    OBDal.getInstance().flush();
    return pendingOp;
  }
  
  public static void saveQCResponse(CustqcPendingOp pendingOp,String qcResponse ,
      String errorLog) {
    OBCriteria<CustqcPendingOp> pendingOpCriteria = OBDal.getInstance()
        .createCriteria(CustqcPendingOp.class);
    final String id = (String) pendingOp.getId();
    pendingOpCriteria.add(Restrictions.eq(CustqcPendingOp.PROPERTY_ID, id));
    List<CustqcPendingOp> qcPendingOpList = pendingOpCriteria.list();
    CustqcPendingOp qcPendingOp = qcPendingOpList.get(0);
    qcPendingOp.setResponse(qcResponse);
    qcPendingOp.setErrorLog(errorLog);
    if(errorLog != null) {    
      qcPendingOp.setProcessed(false);
    } 
    OBDal.getInstance().save(qcPendingOp);
    OBDal.getInstance().flush();
  }
}

