/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jfree.util.Log;
import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.retail.posterminal.OBPOSApplications;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcWebserviceConfig;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetBalanceEnquiry;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.ResultBalanceEnquiry;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;

public class WSBalanceEnquiry extends WSQwikCilverBase {
  private static Logger log = Logger.getLogger(WSBalanceEnquiry.class);
  @Override
  protected WebServiceURL getWebServiceURL(JSONObject jsonData) {
    return Utils.getWebServiceURL(jsonData.optString("organization"),
        CustqcWebserviceConfig.PROPERTY_BALANCEENQUIRYPATH,
        CustqcWebserviceConfig.PROPERTY_BALANCEENQUIRYHTTPMTH,
        "CUSTQC_QwikCilverErrConfigBalanceInquiry");
  }

  @Override
  protected void callQwikCilverServer(JSONObject jsonData, JSONObject data, WebServiceURL wsUrl,
      OBPOSApplications pos) {

    JSONObject parameters = jsonData.optJSONObject("parameters");
    String trackData = parameters.optJSONObject("cardNumber").optString("value");
    String cardNumber = Utils.getCardNumberFromDigitBarCode(trackData);
    Map<String, String> headers = Utils.getCommonHeaders(pos);
    JSONObject body = new JSONObject();
    try {
      body.put("CardNumber", cardNumber);
      if (trackData.length() > 16) {
        body.put("trackData", trackData);
      }
      log.info("Balance Enquiry Request"+body.toString());  
    } catch (JSONException ignore) {
    }
    NetBalanceEnquiry balanceEnquiry = new NetBalanceEnquiry();
    ResultBalanceEnquiry netResult = (ResultBalanceEnquiry) balanceEnquiry.read(wsUrl, headers,
        body);
    Utils.updateTransactionId(pos, netResult);
    boolean isGiftcard = parameters.optJSONObject("isGiftcard").optBoolean("value");
    boolean isReloadGiftcard = parameters.optJSONObject("isReloadGiftcard").optBoolean("value");
    boolean isBalanceEnquiry = parameters.optJSONObject("isBalanceEnquiry").optBoolean("value");
    String cardProgramGroupType = null;
    if (netResult.isSuccess()) {
      if (isGiftcard) {
        throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10015", null));
      }
      if (netResult.getCurrencyConversionRate().equals(0.0)
          && !pos.getOrganization().getCurrency().getISOCode()
              .equals(netResult.getCardCurrencySymbol())) {
        throw new OBException(OBMessageUtils.getI18NMessage(
            "CUSTQC_QwikCilverErrConfigCurrency",
            new String[] { pos.getOrganization().getCurrency().getISOCode(),
                netResult.getCardCurrencySymbol() }));
      }
      log.info("Balance Enquiry Response"+netResult.toString()); 
      SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
      try {
        data.put("cardNumber", cardNumber);
        data.put("cardExpiry", dFormat.format(netResult.getCardExpiry()));
        data.put("amount", netResult.getAmount());
        data.put("currencyConversionRate", netResult.getCurrencyConversionRate());
        data.put("cardCurrencySymbol", netResult.getCardCurrencySymbol());
        data.put("invoiceNumber", netResult.getInvoiceNumber());
        if (netResult.getResponseMessage().split("=").length > 1) {
          cardProgramGroupType = netResult.getResponseMessage().split("=")[1];
          data.put("CardProgramGroupType", cardProgramGroupType);
        }
        data.put("batchNumber", Long.parseLong(Utils.getPOSValue(pos,
            OBPOSApplications.PROPERTY_CUSTQCCURRENTBATCHNUMBER, false)));
        log.info("Balance Enquiry Response"+data.toString());      
        } catch (JSONException ignore) {
      }
    } else if (netResult.getResponseCode().intValue() == ResponseCodes.RESPONSE_CARD_NOT_ACTIVATE) {
      if (isReloadGiftcard || isBalanceEnquiry) {
        throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10029", null));
      }
      try {
        data.put("cardNumber", cardNumber);
        data.put("amount", netResult.getAmount());
        data.put("currencyConversionRate", netResult.getCurrencyConversionRate());
        data.put("cardCurrencySymbol", netResult.getCardCurrencySymbol());
      } catch (JSONException ignore) {
      }
    } else {
      netResult.checkErrors();
    }
  }
}
