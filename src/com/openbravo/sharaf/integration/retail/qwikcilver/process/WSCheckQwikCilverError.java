package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.retail.posterminal.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcPendingOp;

public class WSCheckQwikCilverError extends JSONProcessSimple {

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    JSONObject response = new JSONObject();

    JSONObject parameters = jsonsent.optJSONObject("parameters");
    OBContext.setAdminMode(true);
    try {

      OBCriteria<CustqcPendingOp> pendingOp = OBDal.getInstance().createCriteria(
          CustqcPendingOp.class);
      pendingOp.setFilterOnReadableOrganization(false);
      pendingOp.add(Restrictions.eq(CustqcPendingOp.PROPERTY_CARDNUMBER,
          parameters.optJSONObject("cardNumber").optString("value")));
      pendingOp.add(Restrictions.eq(CustqcPendingOp.PROPERTY_CARDACTION, "REDEEM"));
      pendingOp.add(Restrictions.eq(CustqcPendingOp.PROPERTY_PROCESSED, false));
      pendingOp.add(Restrictions.eq(CustqcPendingOp.PROPERTY_QCPENDINGTXN, true));
      pendingOp.setMaxResults(1);
      if (pendingOp.list().size() > 0) {
        response.put("Card Error", true);
      } else {
        response.put("Card Error", false);
      }

    } finally {
      OBContext.restorePreviousMode();
    }
    JSONObject result = new JSONObject();
    try {
      result.put(JsonConstants.RESPONSE_DATA, response);
      result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (JSONException ignoree) {
    }
    return result;
  }
}
