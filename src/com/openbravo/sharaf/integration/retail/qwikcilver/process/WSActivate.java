/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.core.OBContext;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcPendingOp;
import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcWebserviceConfig;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetActivate;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetReload;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.ResultActivate;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;
import org.apache.log4j.Logger;

// It's no longer in use after OBDEV-182 Changes

public class WSActivate extends WSQwikCilverBase {
  
  private static Logger log = Logger.getLogger(WSActivate.class);

  private String getNotes(boolean isGiftcard, String cardNumber, String amount) {
    return (isGiftcard ? "Activate: " : "Reload: ") + cardNumber + " with: " + amount;
  }

  @Override
  protected WebServiceURL getWebServiceURL(JSONObject jsonData) {
    boolean isGiftcard = jsonData.optBoolean("isGiftcard");
    if (isGiftcard) {
      return Utils.getWebServiceURL(jsonData.optString("organization"),
          CustqcWebserviceConfig.PROPERTY_ACTIVATEPATH,
          CustqcWebserviceConfig.PROPERTY_ACTIVATEHTTPMTH, "CUSTQC_QwikCilverErrConfigActivate");
    } else {
      return Utils.getWebServiceURL(jsonData.optString("organization"),
          CustqcWebserviceConfig.PROPERTY_RELOADPATH,
          CustqcWebserviceConfig.PROPERTY_RELOADHTTPMTH, "CUSTQC_QwikCilverErrConfigReload");
    }
  }

  @Override
  protected void callQwikCilverServer(JSONObject jsonData, JSONObject data, WebServiceURL wsUrl,
      OBPOSApplications pos) {
    boolean isGiftcard = jsonData.optBoolean("isGiftcard");
    ResultActivate netResult;
    Map<String, String> headers = Utils.getCommonHeaders(pos);
    String cardCurrencySymbol = jsonData.optString("cardCurrencySymbol");
    Double amount = jsonData.optDouble("amount");
    String amountFormatted = Utils.getFormattedAmount(cardCurrencySymbol, amount);
    String trackData = jsonData.optString("cardNumber");
    String cardNumber = Utils.getCardNumberFromDigitBarCode(trackData);
    String notes = getNotes(isGiftcard, cardNumber, amountFormatted);
    String idempotencyKey = jsonData.optString("idempotencyKey");
    String errorLog = null;
    boolean isQCPendingTxn = false;
    CustqcPendingOp pendingOp = null;
    CustqcPendingOp qcPendingOp = null;
    JSONObject body = new JSONObject();
    try {
      body.put("CardNumber", cardNumber);
      body.put("InvoiceNumber", jsonData.optString("invoiceNumber"));
      body.put("Amount", amountFormatted);
      body.put("Notes", notes);
      body.put("IdempotencyKey", idempotencyKey);
      if (trackData.length() > 16) {
        body.put("trackData", trackData);
      }
      String qcRequest = body.toString();
      Long batchNumber = Long.parseLong(
          Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCCURRENTBATCHNUMBER, false));        
      pendingOp = Utils.saveQCTxn(isGiftcard ? "ACTIVATE" : "RELOAD", cardNumber, amount, "-", batchNumber,
          jsonData.getString("invoiceNumber"), notes, pos.getCustqcTransactionid(), pos, qcRequest, 
          null , null, isQCPendingTxn);
      if (isGiftcard) {
        JSONObject customer = new JSONObject();
        customer.put("Firstname", jsonData.optString("firstName"));
        customer.put("LastName", jsonData.optString("lastName"));
        customer.put("PhoneNumber", jsonData.optString("phoneNumber"));
        body.put("Customer", customer);
        NetActivate activate = new NetActivate();
        netResult = (ResultActivate) activate.read(wsUrl, headers, body);
      } else {
        NetReload reload = new NetReload();
        netResult = (ResultActivate) reload.read(wsUrl, headers, body);
      }
      Utils.updateTransactionId(pos, netResult);
      data.put("cardNumber", cardNumber);
      if (!netResult.isSuccess()) {
        try {   
          String qcResponse = netResult.getQcResponse();
          errorLog = netResult.getResponseMessage();
          Utils.saveQCResponse(pendingOp, qcResponse, errorLog);
        }catch (Exception e) {
          log.error(e);
        }      
        netResult.checkErrors();
      } else {
        SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        data.put("cardExpiry", dFormat.format(netResult.getCardExpiry()));
        data.put("batchNumber", netResult.getBatchNumber());
        data.put("approvalCode", netResult.getApprovalCode());
        data.put("amount", netResult.getAmount());
        data.put("purchaseAmount", netResult.getPurchaseAmount());
        String refId = netResult.getTransactionId().toString();
        data.put("referenceId", refId);
        data.put("invoiceNumber", netResult.getInvoiceNumber());
        data.put("notes", notes);
        try {   
          String qcResponse = netResult.getQcResponse();
          Utils.saveQCResponse(pendingOp, qcResponse, null);
        }catch (Exception e) {
          log.error(e);
        }    
   }
  } catch (JSONException ignore) {
   } 
  }

  @Override
  protected void processQwickCilverException(JSONObject result, JSONObject jsonData,
      OBPOSApplications pos) {
    try {
      boolean isGiftcard = jsonData.getBoolean("isGiftcard");
      String cardCurrencySymbol = jsonData.getString("cardCurrencySymbol");
      Double amount = jsonData.getDouble("amount");
      String amountFormatted = Utils.getFormattedAmount(cardCurrencySymbol, amount);
      String cardNumber = Utils.getCardNumberFromDigitBarCode(jsonData.getString("cardNumber"));
      Long batchNumber = Long.parseLong(Utils.getPOSValue(pos,
          OBPOSApplications.PROPERTY_CUSTQCCURRENTBATCHNUMBER, false));
      JSONObject data = result.getJSONObject("data");
      data.put("cardNumber", cardNumber);
      data.put("batchNumber", batchNumber);
      data.put("approvalCode", "-");
      data.put("amount", amount);
      data.put("referenceId", 0L);
      data.put("invoiceNumber", jsonData.getString("invoiceNumber"));
    } catch (JSONException e) {
      throw new OBException(
          OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrReadResponse", null), e, true);
    }
  }

}
