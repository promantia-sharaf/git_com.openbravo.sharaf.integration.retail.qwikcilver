/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.process;

public class ResponseCodes {

  public final static int RESPONSE_CARD_NOT_FOUND = 10004;
  public final static int RESPONSE_CARD_ALREADY_ACTIVE = 10015;
  public final static int RESPONSE_CARD_RELOAD_MAX = 10022;
  public final static int RESPONSE_CARD_DESACTIVATED = 10027;
  public final static int RESPONSE_CARD_NOT_ACTIVATE = 10029;
  public final static int RESPONSE_CARD_RELOAD_MIN = 10035;
  public final static int RESPONSE_CARD_LIMIT_MIN = 10036;
  public final static int RESPONSE_CARD_LIMIT_MAX = 10038;
  public final static int RESPONSE_INVALID_BATCH = 10064;
  public final static int RESPONSE_REPLACED_DESACTIVATED = 10071;
  public final static int RESPONSE_CARD_ALREADY_REISSUED = 10075;
  public final static int RESPONSE_ALREADY_CANCELLED = 10084;
  public final static int RESPONSE_CARD_INVALID = 10086;
  public final static int RESPONSE_CANCEL_EXCEED_AMOUNT = 10105;
  public final static int RESPONSE_INVALID_DECIMAL_PLACES = 10115;
  public final static int RESPONSE_CARDNUMBER_NOTMATCH_TRACKDATA = 10193;
  public final static int RESPONSE_CARD_INSUFFICIENT_BALANCE = 10010;
  public final static int RESPONSE_CARD_ACTIVATED_AND_ALREADY_REDEEMED = 10260;
}
