/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.retail.qwikcilver;

import java.text.SimpleDateFormat;

import javax.enterprise.context.ApplicationScoped;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.retail.posterminal.OrderLoaderPreProcessPaymentHook;

@ApplicationScoped
public class CUSTQCOrderLoaderPreProcessPaymentHook extends OrderLoaderPreProcessPaymentHook {
  public void exec(JSONObject jsonorder, Order order, JSONObject jsonpayment, FIN_Payment payment)
      throws Exception {

    if (jsonpayment.has("paymentData")) {
      JSONObject paymentData = jsonpayment.getJSONObject("paymentData");
      if (paymentData.has("QwikCilverTransaction")
          && "DONE".equals(paymentData.getString("QwikCilverTransaction"))) {
        String cardExpiry = paymentData.getString("CardExpiry");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        payment.setCUSTQCCardNumber(paymentData.getString("CardNumber"));
        payment.setCUSTQCExpirationDate(formatter.parse(cardExpiry));
        payment.setCUSTQCBatchNumber(paymentData.getLong("BatchId"));
        payment.setCUSTQCTransactionID(paymentData.getLong("ReferenceId"));
        payment.setCUSTQCApprovalCode(paymentData.getString("ApprovalCode"));
      }
    }
  }
}