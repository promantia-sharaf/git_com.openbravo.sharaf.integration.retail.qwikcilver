/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.retail.qwikcilver;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.OrderLoaderHook;
import org.openbravo.retail.posterminal.POSUtils;

import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcPendingOp;
import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcWebserviceConfig;
import com.openbravo.sharaf.integration.retail.qwikcilver.process.Utils;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetActivate;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.NetReload;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.ResultActivate;
import com.openbravo.sharaf.integration.retail.qwikcilver.webservices.WebServiceURL;

public class CUSTQCOrderLoaderHook implements OrderLoaderHook {

  private static Logger log = Logger.getLogger(CUSTQCOrderLoaderHook.class);

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {
    log.info("JSON Order Orderloader Hook" + jsonorder.toString());
    log.info("Order Orderloader Hook" + order.toString());
    //OBDEV-182 Changes
    String posid = null;
    JSONObject data = new JSONObject();
    JSONArray orderLines = jsonorder.getJSONArray("lines");
    WebServiceURL wsUrl = null;

    for (int i = 0; i < orderLines.length(); i++) {
      JSONObject line = orderLines.getJSONObject(i);
      log.info("Qwikcilver Call" + line.toString());
      boolean isGiftcard = line.optBoolean("custqcIsgiftcard");
      boolean isReloadGiftcard = line.optBoolean("custqcIsreloadgiftcard");
      if(isGiftcard || isReloadGiftcard) {
        if (isGiftcard) {
          wsUrl = Utils.getWebServiceURL(jsonorder.optString("organization"),
              CustqcWebserviceConfig.PROPERTY_ACTIVATEPATH,
              CustqcWebserviceConfig.PROPERTY_ACTIVATEHTTPMTH, "CUSTQC_QwikCilverErrConfigActivate");
        } else {
          wsUrl = Utils.getWebServiceURL(jsonorder.optString("organization"),
              CustqcWebserviceConfig.PROPERTY_RELOADPATH,
              CustqcWebserviceConfig.PROPERTY_RELOADHTTPMTH, "CUSTQC_QwikCilverErrConfigReload");
        }
        ResultActivate netResult;
        OBPOSApplications pos = OBDal.getInstance().get(OBPOSApplications.class,
            jsonorder.getString("posTerminal"));
        Map<String, String> headers = Utils.getCommonHeaders(pos);
        String cardCurrencySymbol = line.optString("custqcCardCurrencySymbol");
        Double amount = line.optDouble("discountedGross");
        String amountFormatted = Utils.getFormattedAmount(cardCurrencySymbol, amount);
        String trackData = line.optString("custqcRealCardnumber");
        String cardNumber = Utils.getCardNumberFromDigitBarCode(trackData);
        String notes = getNotes(isGiftcard, cardNumber, amountFormatted);
        String idempotencyKey = jsonorder.optString("documentNo") + "-"
            + line.get("custqcRealCardnumber") + "-" + line.get("id");
        String errorLog = null;
        boolean isQCPendingTxn = false;
        CustqcPendingOp pendingOp = null;
        CustqcPendingOp qcPendingOp = null;
        JSONObject body = new JSONObject();
        try {
          body.put("CardNumber", cardNumber);
          body.put("InvoiceNumber", jsonorder.optString("documentNo"));
          body.put("Amount", amountFormatted);
          body.put("Notes", notes);
          body.put("IdempotencyKey", idempotencyKey);
          if (trackData.length() > 16) {
            body.put("trackData", trackData);
          }
          String qcRequest = body.toString();
          log.info("WS Activate Request" + body.toString());
          Long batchNumber = Long.parseLong(
              Utils.getPOSValue(pos, OBPOSApplications.PROPERTY_CUSTQCCURRENTBATCHNUMBER, false));
          log.info("Batch number" + batchNumber.toString());
          log.info("POS Transaction ID" + pos.getCustqcTransactionid());
          
          log.info("Saving QC trans" + cardNumber);
          pendingOp = Utils.saveQCTxn(isGiftcard ? "ACTIVATE" : "RELOAD", cardNumber, amount, "-", batchNumber,
              jsonorder.optString("documentNo"), notes, pos.getCustqcTransactionid(), pos, qcRequest, 
              null , null, isQCPendingTxn);
          log.info("After QC Transaction");
          if (isGiftcard) {
            JSONObject customer = new JSONObject();
            customer.put("Firstname", line.optString("firstName"));
            customer.put("LastName", line.optString("lastName"));
            customer.put("PhoneNumber", line.optString("phoneNumber"));
            body.put("Customer", customer);
            NetActivate activate = new NetActivate();
            netResult = (ResultActivate) activate.read(wsUrl, headers, body);
          } else {
            NetReload reload = new NetReload();
            netResult = (ResultActivate) reload.read(wsUrl, headers, body);
          }
          Utils.updateTransactionId(pos, netResult);
          data.put("cardNumber", cardNumber);
          if (!netResult.isSuccess()) {
            try {
              String qcResponse = netResult.getQcResponse();
              errorLog = netResult.getResponseMessage();
              Utils.saveQCResponse(pendingOp, qcResponse, errorLog);
            } catch (Exception e) {
              log.error(e);
            }
            //netResult.checkErrors();
          } else {
            log.info("Net Result" + netResult.getInvoiceNumber());
            SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            data.put("cardExpiry", dFormat.format(netResult.getCardExpiry()));
            data.put("batchNumber", netResult.getBatchNumber());
            data.put("approvalCode", netResult.getApprovalCode());
            data.put("amount", netResult.getAmount());
            data.put("purchaseAmount", netResult.getPurchaseAmount());
            String refId = netResult.getTransactionId().toString();
            data.put("referenceId", refId);
            data.put("invoiceNumber", netResult.getInvoiceNumber());
            data.put("notes", notes);
            try {
              String qcResponse = netResult.getQcResponse();
              Utils.saveQCResponse(pendingOp, qcResponse, null);
            } catch (Exception e) {
              log.error(e);
            }
          }
        } catch (JSONException ignore) {
        }
      }
    }
    if (jsonorder.has("qwikCilverPaymentErrors")) {
      saveErrors(jsonorder.getJSONArray("qwikCilverPaymentErrors"),
          jsonorder.getString("posTerminal"));
    }
    if (jsonorder.has("qwikCilverLineErrors")) {
      saveErrors(jsonorder.getJSONArray("qwikCilverLineErrors"),
          jsonorder.getString("posTerminal"));
    }
  }

  private String getNotes(boolean isGiftcard, String cardNumber, String amount) {
    return (isGiftcard ? "Activate: " : "Reload: ") + cardNumber + " with: " + amount;
  }

  private void saveErrors(JSONArray errors, String terminalId) throws Exception {
    OBPOSApplications terminal = POSUtils.getTerminalById(terminalId);
    for (int i = 0; i < errors.length(); i++) {
      JSONObject error = errors.getJSONObject(i);
      String cardNumber = Utils.getCardNumberFromDigitBarCode(error.getString("cardNumber"));
      Utils.saveQCTxn(error.getString("operation"), cardNumber, error.getDouble("amount"),
          error.getString("approvalCode"), error.getLong("batchNumber"),
          error.getString("invoiceNumber"), error.getString("notes"),
          error.has("referenceId") ? error.getLong("referenceId") : 0L, terminal, null, null,
          error.has("errorLog") ? error.getString("errorLog") : null,
          error.has("isPendingTxn") ? error.getBoolean("isPendingTxn") : true);
    }
  }
}
