/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public abstract class NetBalanceEnquiryBase extends NetClient {

  @Override
  protected WebServiceResult getServiceResult() {
    return new ResultBalanceEnquiry(-1L, "Invalid WebService call");
  }

  protected void processResponse(ResultBalanceEnquiry result, JSONObject response)
      throws JSONException {
    result.setCardNumber(response.getString("CardNumber"));
    result.setAmount(response.getDouble("Amount"));
    result.setInvoiceNumber(response.getString("InvoiceNumber"));
    result.setCardCurrencySymbol(response.getString("CardCurrencySymbol"));
    result.setCurrencyConversionRate(response.getDouble("CurrencyConversionRate"));
    SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    try {
      result.setCardExpiry(dFormat.parse(response.getString("CardExpiry")));
    } catch (ParseException e) {
      result.setCardExpiry(null);
    }
    String cardProgramGroupType = (response.getString("CardProgramGroupType") == null) ? ""
        : response.getString("CardProgramGroupType");
    result.setCardProgramGroupType(cardProgramGroupType);
  }

}
