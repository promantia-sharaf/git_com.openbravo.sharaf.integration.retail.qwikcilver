/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;

import com.openbravo.sharaf.integration.retail.qwikcilver.process.ResponseCodes;

public class ResultCancelRedeem extends ResultRedeem {

  public ResultCancelRedeem(Long responseCode, String responseMessage) {
    super(responseCode, responseMessage);
  }

  @Override
  public void checkErrors() {
    int respCode = getResponseCode().intValue();
    switch (respCode) {
    // Amount being cancelled is exceeding the original transaction amount
    case ResponseCodes.RESPONSE_CANCEL_EXCEED_AMOUNT:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10105", null));

      // Transaction is already cancelled
    case ResponseCodes.RESPONSE_ALREADY_CANCELLED:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10084", null));
    }
    super.checkErrors();
  }

}
