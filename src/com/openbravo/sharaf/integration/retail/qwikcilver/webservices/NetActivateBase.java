/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.apache.log4j.Logger;

public abstract class NetActivateBase extends NetClient {
  
  private static Logger log = Logger.getLogger(NetActivateBase.class);
  @Override
  protected WebServiceResult getServiceResult() {
    return new ResultActivate(-1L, "Invalid WebService call");
  }

  protected void processResponse(ResultActivate pResult, JSONObject response) throws JSONException {
    pResult.setAmount(response.getDouble("Amount"));
    pResult.setApprovalCode(response.getString("ApprovalCode"));
    pResult.setInvoiceNumber(response.getString("InvoiceNumber"));
    pResult.setReferenceId(response.getString("TransactionId"));
    pResult
        .setBatchNumber(response.getJSONObject("ApiWebProperties").getLong("CurrentBatchNumber"));
    SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    pResult.setQcResponse(response.toString());
    try {
      pResult.setCardExpiry(dFormat.parse(response.getString("CardExpiry")));
    } catch (ParseException e) {
      pResult.setCardExpiry(null);
    }
  }

}
