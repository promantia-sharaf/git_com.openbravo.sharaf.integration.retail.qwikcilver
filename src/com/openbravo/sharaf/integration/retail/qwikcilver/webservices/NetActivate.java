/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.apache.log4j.Logger;

public class NetActivate extends NetActivateBase {

  private static Logger log = Logger.getLogger(NetActivate.class);
  @Override
  protected void processResponse(WebServiceResult result, JSONObject response) throws JSONException {
    ResultActivate pResult = (ResultActivate) result;
    processResponse(pResult, response);
    pResult.setCardNumber(response.getString("CardNumber"));
    pResult.setPurchaseAmount(response.has("Amount") ? response.getDouble("Amount")
        : 0.0);
    pResult.setQcResponse(response.toString());
  }

}
