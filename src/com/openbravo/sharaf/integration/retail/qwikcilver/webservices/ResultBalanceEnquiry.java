/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import java.util.Date;

import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;

import com.openbravo.sharaf.integration.retail.qwikcilver.process.ResponseCodes;

public class ResultBalanceEnquiry extends WebServiceResult {

  Date cardExpiry;
  String cardNumber;
  String invoiceNumber;
  Double amount;
  Double currencyConversionRate;
  String cardCurrencySymbol;
  String cardProgramGroupType;

  public ResultBalanceEnquiry(Long responseCode, String responseMessage) {
    super(responseCode, responseMessage);
  }

  @Override
  public void checkErrors() {
    int respCode = getResponseCode().intValue();
    switch (respCode) {
    // Card already reissued
    case ResponseCodes.RESPONSE_CARD_ALREADY_REISSUED:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10075", null));

      // Card not activated
    case ResponseCodes.RESPONSE_CARD_NOT_ACTIVATE:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10029", null));

      // The replacement card has been deactivated
    case ResponseCodes.RESPONSE_REPLACED_DESACTIVATED:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10071", null));

      // Card is deactivated.
    case ResponseCodes.RESPONSE_CARD_DESACTIVATED:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10027", null));

      // Entered amount is greater than the configured decimal places
    case ResponseCodes.RESPONSE_INVALID_DECIMAL_PLACES:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10115", null));
    }
    super.checkErrors();
  }

  public Date getCardExpiry() {
    return cardExpiry;
  }

  public void setCardExpiry(Date cardExpiry) {
    this.cardExpiry = cardExpiry;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getInvoiceNumber() {
    return invoiceNumber;
  }

  public void setInvoiceNumber(String invoiceNumber) {
    this.invoiceNumber = invoiceNumber;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Double getCurrencyConversionRate() {
    return currencyConversionRate;
  }

  public void setCurrencyConversionRate(Double currencyConversionRate) {
    this.currencyConversionRate = currencyConversionRate;
  }

  public String getCardCurrencySymbol() {
    return cardCurrencySymbol;
  }

  public void setCardCurrencySymbol(String cardCurrencySymbol) {
    this.cardCurrencySymbol = cardCurrencySymbol;
  }

  public String getCardProgramGroupType() {
    return cardProgramGroupType;
  }

  public void setCardProgramGroupType(String cardProgramGroupType) {
    this.cardProgramGroupType = cardProgramGroupType;
  }

}
