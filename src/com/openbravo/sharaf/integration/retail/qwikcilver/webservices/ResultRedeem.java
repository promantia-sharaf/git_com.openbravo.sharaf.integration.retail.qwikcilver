/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

public class ResultRedeem extends ResultBalanceEnquiry {

  Long batchNumber;
  String referenceId;
  String approvalCode;
  Double previousBalance;
  String qcResponse;
  
  public ResultRedeem(Long responseCode, String responseMessage) {
    super(responseCode, responseMessage);
  }

  public Long getBatchNumber() {
    return batchNumber;
  }

  public void setBatchNumber(Long batchNumber) {
    this.batchNumber = batchNumber;
  }

  public String getReferenceId() {
    return referenceId;
  }

  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }

  public String getApprovalCode() {
    return approvalCode;
  }

  public void setApprovalCode(String approvalCode) {
    this.approvalCode = approvalCode;
  }

  public Double getPreviousBalance() {
    return previousBalance;
  }

  public void setPreviousBalance(Double previousBalance) {
    this.previousBalance = previousBalance;
  }

  public Double getRedeemAmount() {
    return previousBalance - amount;
  }
  
  public String getQcResponse() {
    return qcResponse;
  }

  public void setQcResponse(String qcResponse) {
    this.qcResponse = qcResponse;
  }  

}
