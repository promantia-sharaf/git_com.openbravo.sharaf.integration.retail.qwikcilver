/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class NetBalanceTransfer extends NetBalanceEnquiryBase {

  @Override
  protected void processResponse(WebServiceResult result, JSONObject response) throws JSONException {
    ResultBalanceEnquiry pResult = (ResultBalanceEnquiry) result;
    processResponse(pResult, response);
  }

}
