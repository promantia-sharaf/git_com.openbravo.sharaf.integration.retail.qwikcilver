/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openbravo.sharaf.integration.retail.qwikcilver.process.QwickCilverException;
import com.openbravo.sharaf.integration.retail.qwikcilver.process.Utils;

public abstract class NetClient {
  private static final Logger log = LoggerFactory.getLogger(NetClient.class);

  public WebServiceResult read(WebServiceURL wsUrl, Map<String, String> headers, JSONObject body)
      throws OBException {
    WebServiceResult result = getServiceResult();
    String output = "";
    try {

      URL url = new URL(wsUrl.getUrl());
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod(wsUrl.getMethod());
      conn.setRequestProperty("Accept", "application/json");
      conn.setRequestProperty("Content-Type", "application/json");

      System.setProperty("https.protocols", "TLSv1.2");

      Iterator<?> it = headers.entrySet().iterator();
      while (it.hasNext()) {
        @SuppressWarnings("rawtypes")
        Map.Entry pair = (Map.Entry) it.next();
        conn.setRequestProperty((String) pair.getKey(), (String) pair.getValue());
      }

      log.debug("Body contains: " + body);
      if (body != null) {
        conn.setDoOutput(true);
        String input = getJsonStringWithNullValues(body);
        log.debug("Request json object: {} ", body.toString(2));

        OutputStream os = conn.getOutputStream();
        os.write(input.getBytes());
        os.flush();
      }
      if (conn.getResponseCode() != 200) {
        throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
      }

      BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

      String line;
      while ((line = br.readLine()) != null) {
        output += line;
      }
      conn.disconnect();
      log.debug("Received resposne: {} ", output);

      processResponse(result, output);
      Utils.checkCommonErrors(result);
    } catch (MalformedURLException e) {
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrInvalidURL",
          new String[] { wsUrl.getUrl() }));
    } catch (UnknownHostException e) {
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrInvalidURL",
          new String[] { wsUrl.getUrl() }));
    } catch (SocketException e) {
      throw new QwickCilverException();
    } catch (IOException e) {
      throw new OBException(
          OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrReadResponse", null), e, true);
    } catch (JSONException e) {
      log.error("Error parsing received json {}", output, e);
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErrProcessingResponse",
          null));
    }
    return result;
  }

  /**
   * Parse JSONObject to string including null values
   */
  private static String getJsonStringWithNullValues(JSONObject obj) {
    String result = obj.toString();
    result = result.replaceAll("\"null\"", "null");
    return result;
  }

  public WebServiceResult read(WebServiceURL wsUrl, Map<String, String> headers) throws OBException {
    return read(wsUrl, headers, null);
  }

  protected void processResponse(WebServiceResult result, String response) throws JSONException {
    JSONObject jsonResponse = new JSONObject(response);
    result.setResponseCode(jsonResponse.getLong("ResponseCode"));
    result.setResponseMessage(jsonResponse.getString("ResponseMessage"));
    result.setTransactionId(jsonResponse.getLong("TransactionId"));
    processResponse(result, jsonResponse);
  }

  abstract protected WebServiceResult getServiceResult();

  abstract protected void processResponse(WebServiceResult result, JSONObject response)
      throws JSONException;

}
