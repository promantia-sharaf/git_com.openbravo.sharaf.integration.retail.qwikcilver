/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.openbravo.sharaf.integration.retail.qwikcilver.process.FetchAutoSettlementForReturn;

import org.apache.log4j.Logger;

public class NetRedeem extends NetBalanceEnquiryBase {
  
  private static Logger log = Logger.getLogger(NetRedeem.class);

  @Override
  protected WebServiceResult getServiceResult() {
    return new ResultRedeem(-1L, "Invalid WebService call");
  }

  @Override
  protected void processResponse(WebServiceResult result, JSONObject response) throws JSONException {
    ResultRedeem pResult = (ResultRedeem) result;
    processResponse(pResult, response);
    pResult
        .setBatchNumber(response.getJSONObject("ApiWebProperties").getLong("CurrentBatchNumber"));
    pResult.setReferenceId(response.getString("TransactionId"));
    pResult.setApprovalCode(response.getString("ApprovalCode"));
    pResult.setPreviousBalance(response.getDouble("PreviousBalance"));
    pResult.setQcResponse(response.toString());
  }

}
