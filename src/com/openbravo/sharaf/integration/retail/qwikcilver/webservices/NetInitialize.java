/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class NetInitialize extends NetClient {

  @Override
  protected WebServiceResult getServiceResult() {
    return new ResultInitialize(-1L, "Invalid WebService call");
  }

  @Override
  protected void processResponse(WebServiceResult result, JSONObject response) throws JSONException {
    ResultInitialize pResult = (ResultInitialize) result;
    JSONObject apiWebProperties = response.getJSONObject("ApiWebProperties");
    pResult.setMerchantOutletName(apiWebProperties.getString("MerchantOutletName"));
    pResult.setAcquirerId(apiWebProperties.getString("AcquirerId"));
    pResult.setOrganizationName(apiWebProperties.getString("OrganizationName"));
    pResult.setPosEntryMode(apiWebProperties.getString("POSEntryMode"));
    pResult.setPosTypeId(apiWebProperties.getString("POSTypeId"));
    pResult.setPosName(apiWebProperties.getString("POSName"));
    pResult.setTermAppVersion(apiWebProperties.getString("TermAppVersion"));
    pResult.setCurrentBatchNumber(apiWebProperties.getString("CurrentBatchNumber"));
    pResult.setDateAtClient(apiWebProperties.getString("DateAtClient"));
  }

}
