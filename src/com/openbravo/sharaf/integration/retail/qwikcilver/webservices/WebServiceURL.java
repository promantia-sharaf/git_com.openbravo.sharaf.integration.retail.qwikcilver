/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

public class WebServiceURL {

  String url;
  String method;

  public WebServiceURL(String url, String method) {
    super();
    this.url = url;
    this.method = method;
  }

  public String getUrl() {
    return url;
  }

  public String getMethod() {
    return method;
  }

}
