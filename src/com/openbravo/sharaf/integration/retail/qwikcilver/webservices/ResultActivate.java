/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import java.util.Date;

import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;

import com.openbravo.sharaf.integration.retail.qwikcilver.process.ResponseCodes;

public class ResultActivate extends WebServiceResult {

  Date cardExpiry;
  String cardNumber;
  Double amount;
  Double purchaseAmount;
  Long batchNumber;
  String referenceId;
  String approvalCode;
  String invoiceNumber;
  String qcResponse;

  public ResultActivate(Long responseCode, String responseMessage) {
    super(responseCode, responseMessage);
  }

  public void checkErrors() {
    int respCode = getResponseCode().intValue();
    switch (respCode) {
    // Card already active
    case ResponseCodes.RESPONSE_CARD_ALREADY_ACTIVE:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10015", null));

      // Amount more than max reload limit
    case ResponseCodes.RESPONSE_CARD_RELOAD_MAX:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10022", null));

      // Amount less than min reload limit
    case ResponseCodes.RESPONSE_CARD_RELOAD_MIN:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10035", null));

      // Amount less than min load limit
    case ResponseCodes.RESPONSE_CARD_LIMIT_MIN:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10036", null));

      // Amount more than max load limit
    case ResponseCodes.RESPONSE_CARD_LIMIT_MAX:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10038", null));

      // Entered amount is greater than the configured decimal places.
    case ResponseCodes.RESPONSE_INVALID_DECIMAL_PLACES:
      throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10115", null));
    }
    super.checkErrors();
  }

  public Date getCardExpiry() {
    return cardExpiry;
  }

  public void setCardExpiry(Date cardExpiry) {
    this.cardExpiry = cardExpiry;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Double getPurchaseAmount() {
    return purchaseAmount;
  }

  public void setPurchaseAmount(Double purchaseAmount) {
    this.purchaseAmount = purchaseAmount;
  }

  public Long getBatchNumber() {
    return batchNumber;
  }

  public void setBatchNumber(Long batchNumber) {
    this.batchNumber = batchNumber;
  }

  public String getReferenceId() {
    return referenceId;
  }

  public void setReferenceId(String referenceId) {
    this.referenceId = referenceId;
  }

  public String getApprovalCode() {
    return approvalCode;
  }

  public void setApprovalCode(String approvalCode) {
    this.approvalCode = approvalCode;
  }

  public String getInvoiceNumber() {
    return invoiceNumber;
  }

  public void setInvoiceNumber(String invoiceNumber) {
    this.invoiceNumber = invoiceNumber;
  }
  
  public String getQcResponse() {
    return qcResponse;
  }

  public void setQcResponse(String qcResponse) {
    this.qcResponse = qcResponse;
  }  


}
