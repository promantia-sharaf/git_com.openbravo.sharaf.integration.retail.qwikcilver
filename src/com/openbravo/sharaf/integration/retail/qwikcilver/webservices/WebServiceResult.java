/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

import org.openbravo.base.exception.OBException;
import org.openbravo.erpCommon.utility.OBMessageUtils;

import com.openbravo.sharaf.integration.retail.qwikcilver.process.ResponseCodes;

public class WebServiceResult {
  Long responseCode;
  String responseMessage;
  Long transactionId;

  public WebServiceResult(Long responseCode, String responseMessage) {
    super();
    this.responseCode = responseCode;
    this.responseMessage = responseMessage;
  }

  public void clearError() {
    this.responseCode = 0L;
    this.responseMessage = "Transaction successful.";
  }

  public void checkErrors() {
    if (responseCode != 0) {
      switch (responseCode.intValue()) {
      // Card already reissued
      case ResponseCodes.RESPONSE_CARDNUMBER_NOTMATCH_TRACKDATA:
        throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_10193", null));
      default:
        throw new OBException(OBMessageUtils.getI18NMessage("CUSTQC_QwikCilverErr_Generic",
            new String[] { responseCode + " - " + responseMessage }));
      }
    }
  }

  public Long getResponseCode() {
    return responseCode;
  }

  public void setResponseCode(Long responseCode) {
    this.responseCode = responseCode;
  }

  public String getResponseMessage() {
    return responseMessage;
  }

  public void setResponseMessage(String responseMessage) {
    this.responseMessage = responseMessage;
  }

  public boolean isSuccess() {
    return responseCode == 0;
  }

  public Long getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(Long transactionId) {
    this.transactionId = transactionId;
  }
}
