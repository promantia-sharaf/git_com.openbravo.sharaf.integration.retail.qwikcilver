/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.qwikcilver.webservices;

public class ResultInitialize extends WebServiceResult {

  String merchantOutletName;
  String acquirerId;
  String organizationName;
  String posEntryMode;
  String posTypeId;
  String posName;
  String termAppVersion;
  String currentBatchNumber;
  String dateAtClient;

  public ResultInitialize(Long responseCode, String responseMessage) {
    super(responseCode, responseMessage);
  }

  public String getMerchantOutletName() {
    return merchantOutletName;
  }

  public void setMerchantOutletName(String merchantOutletName) {
    this.merchantOutletName = merchantOutletName;
  }

  public String getAcquirerId() {
    return acquirerId;
  }

  public void setAcquirerId(String acquirerId) {
    this.acquirerId = acquirerId;
  }

  public String getOrganizationName() {
    return organizationName;
  }

  public void setOrganizationName(String organizationName) {
    this.organizationName = organizationName;
  }

  public String getPosEntryMode() {
    return posEntryMode;
  }

  public void setPosEntryMode(String posEntryMode) {
    this.posEntryMode = posEntryMode;
  }

  public String getPosTypeId() {
    return posTypeId;
  }

  public void setPosTypeId(String posTypeId) {
    this.posTypeId = posTypeId;
  }

  public String getPosName() {
    return posName;
  }

  public void setPosName(String posName) {
    this.posName = posName;
  }

  public String getTermAppVersion() {
    return termAppVersion;
  }

  public String getCurrentBatchNumber() {
    return currentBatchNumber;
  }

  public void setCurrentBatchNumber(String currentBatchNumber) {
    this.currentBatchNumber = currentBatchNumber;
  }

  public void setTermAppVersion(String termAppVersion) {
    this.termAppVersion = termAppVersion;
  }

  public String getDateAtClient() {
    return dateAtClient;
  }

  public void setDateAtClient(String dateAtClient) {
    this.dateAtClient = dateAtClient;
  }

}
