/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

// Add a new property to product's model
OB.Model.Product.addProperties([{
  name: 'custqcIsgiftcard',
  column: 'custqcIsgiftcard',
  primaryKey: false,
  filter: false,
  type: 'BOOL'
}, {
  name: 'custqcIsreloadgiftcard',
  column: 'custqcIsreloadgiftcard',
  primaryKey: false,
  filter: false,
  type: 'BOOL'
}, {
  name: 'custqcDigiApiResponse',
  column: 'custqcDigiApiResponse',
  primaryKey: false,
  filter: false,
  type: 'TEXT'
}]);