/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.CUSTQC = OB.CUSTQC || {};

  OB.CUSTQC.Utils = {

    getProperty: function (properties, propName) {
      var property = _.find(properties, function (prop) {
        return prop.newAttribute.name === propName;
      });
      return property ? property.$.newAttribute.children[0] : property;
    },
/*
    removeLine: function (receipt, line, callback) {
      enyo.$.scrim.show();
      var serverStatus = line.get('custqcServerStatus'),
          cardNumber = line.get('custqcCardnumber'),
          amount = (!OB.UTIL.isNullOrUndefined(line.get('custqcPurchaseamount'))) ? line.get('custqcPurchaseamount') : line.get('custqcAmount'),
          isGiftcard = line.get('custqcIsgiftcard'),
          isReloadGiftcard = line.get('custqcIsreloadgiftcard');
      line.set('custqcExecuteProcess', serverStatus !== 'DONE');
      if (serverStatus === 'PENDING') {
        enyo.$.scrim.hide();
        callback(true);
      } else {
        var processCancelActivate = new OB.DS.Process('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSCancelActivate');
        processCancelActivate.exec({
          cardNumber: cardNumber,
          amount: amount,
          cardCurrencySymbol: line.get('custqcCardCurrencySymbol'),
          currencyConversionRate: line.get('custqcCurrencyConversionRate'),
          approvalCode: line.get('custqcApprovalcode'),
          batchNumber: line.get('custqcBatchnumber'),
          invoiceNumber: line.get('custqcInvoiceNumber'),
          referenceId: line.get('custqcReferenceid'),
          isGiftcard: isGiftcard,
          isReloadGiftcard: isReloadGiftcard
        }, function (response) {
          line.set('custqcExecuteProcess', true);
          if (response && !response.exception) {
            line.set('custqcServerStatus', 'DONE');
            enyo.$.scrim.hide();
            callback(true);
          } else {
            enyo.$.scrim.hide();
            callback(false);
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), response.exception.message);
          }
        }, function (err) {
          line.set('custqcExecuteProcess', true);
          if (err && err.exception) {
            if (err.exception.status.timeout) {
              line.set('custqcServerStatus', 'PENDING');
              OB.CUSTQC.Utils.addGiftCardError(receipt, 'qwikCilverLineErrors', {
                cardNumber: cardNumber,
                amount: amount,
                approvalCode: line.get('custqcApprovalcode'),
                batchNumber: line.get('custqcBatchnumber'),
                invoiceNumber: line.get('custqcInvoiceNumber'),
                referenceId: line.get('custqcReferenceid'),
                notes: "Cancel " + (isGiftcard ? "Activate: " : "Reload: ") + cardNumber + " with: " + amount
              }, isGiftcard ? 'CANCEL_ACTIVATE' : 'CANCEL_RELOAD');
              enyo.$.scrim.hide();
              callback(true);
            } else {
              OB.CUSTQC.Utils.addGiftCardError(receipt, 'qwikCilverLineErrors', {
                cardNumber: cardNumber,
                amount: amount,
                approvalCode: line.get('custqcApprovalcode'),
                batchNumber: line.get('custqcBatchnumber'),
                invoiceNumber: line.get('custqcInvoiceNumber'),
                referenceId: line.get('custqcReferenceid'),
                notes: "Error in Cancel " + (isGiftcard ? "Activate: " : "Reload: ") + cardNumber + " with: " + amount
              }, isGiftcard ? 'CANCEL_ACTIVATE' : 'CANCEL_RELOAD');
              enyo.$.scrim.hide();
              callback(true);
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), err.exception.message);
            }
          }
        });
      }
    },

    finishHook: function (orderLines) {
      var finish = true;
      _.each(orderLines, function (line) {
        if (line.get('custqcIsgiftcard') || line.get('custqcIsreloadgiftcard')) {
          if (!line.get('custqcExecuteProcess')) {
            finish = false;
          }
        }
      });
      return finish;
    },

    removeLines: function (args, callbacks, receipt) {
      var hasGiftCard = false;
      _.each(receipt.get('lines').models, function (line) {
        if (line.get('custqcIsgiftcard') || line.get('custqcIsreloadgiftcard')) {
          hasGiftCard = true;
          if (!OB.MobileApp.model.get('connectedToERP')) {
            OB.info("Qwikcilver Model: id"+' '+OB.UTIL.get_UUID(),"cardNumber"+' '+line.get('custqcCardnumber'),"amount"+' '+(!OB.UTIL.isNullOrUndefined(line.get('custqcPurchaseamount')) ? line.get('custqcPurchaseamount') : line.get('custqcAmount')),"cardCurrencySymbol"+' '
            +line.get('custqcCardCurrencySymbol'),"currencyConversionRate"+' '+line.get('custqcCurrencyConversionRate'),"approvalCode"+' '+line.get('custqcApprovalcode'),"batchNumber"+' '+line.get('custqcBatchnumber'),"invoiceNumber"
            +' '+line.get('custqcInvoiceNumber'),"reference"+' '+line.get('custqcReferenceid'),"isGiftcard"+' '+line.get('custqcIsgiftcard'),"isReloadGiftcard"+' '+line.get('custqcIsreloadgiftcard'),"pos"+' '+OB.MobileApp.model.attributes.terminal.id);            
            var query = "insert into CUSTQC_Qwikcilver(id, cardNumber, amount, cardCurrencySymbol, currencyConversionRate, approvalCode, batchNumber, invoiceNumber, reference, isGiftcard, isReloadGiftcard, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + line.get('custqcCardnumber') + "',"
                          + "'" + (!OB.UTIL.isNullOrUndefined(line.get('custqcPurchaseamount')) ? line.get('custqcPurchaseamount') : line.get('custqcAmount')) + "',"
                          + "'" + line.get('custqcCardCurrencySymbol') + "',"
                          + "'" + line.get('custqcCurrencyConversionRate') + "',"
                          + "'" + line.get('custqcApprovalcode') + "',"
                          + "'" + line.get('custqcBatchnumber') + "',"
                          + "'" + line.get('custqcInvoiceNumber') + "',"
                          + "'" + line.get('custqcReferenceid') + "',"
                          + "'" + line.get('custqcIsgiftcard') + "',"
                          + "'" + line.get('custqcIsreloadgiftcard') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
            OB.Dal.queryUsingCache(OB.Model.CUSTQC_Qwikcilver, query, [], function (status) {
                OB.CUSTQC.Utils.removeLine(receipt, line, function (success) {
                if (success) {
                  if (OB.CUSTQC.Utils.finishHook(args.selectedLines)) {
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                  }
                } else {
                  args.cancelOperation = true;
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
              });
            });
          } else {
              OB.CUSTQC.Utils.removeLine(receipt, line, function (success) {
                if (success) {
                  if (OB.CUSTQC.Utils.finishHook(args.selectedLines)) {
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                  }
                } else {
                  args.cancelOperation = true;
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
              });
          }
        }
      });
      if (!hasGiftCard) {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    },*/

    addGiftCardError: function (receipt, property, data, operation) {
      var errors = receipt.get(property);
      if (!errors) {
        errors = [];
        receipt.set(property, errors);
      }
      errors.push({
        operation: operation,
        cardNumber: data.cardNumber,
        invoiceNumber: data.invoiceNumber,
        approvalCode: data.approvalCode,
        amount: data.amount,
        batchNumber: data.batchNumber,
        referenceId: data.referenceId,
        notes: data.notes,
        errorLog: data.errorLog,
        isPendingTxn: data.isPendingTxn
      });
    },

    getReceiptNotes: function (receipt, callback) {
      var notes = [];
      var swipeValue=0, payModes=[];
      
      var finish = function () {
          var note = _.find(notes, function (n) {
            return n.brand === null || n.department === null;
          });
          if (!note) {
            var result = '';
            _.each(notes, function (note) {
              result += '{ItemCode~' + note.searchkey + '|BrandCode~' + note.brand + '|Department~' + note.department + '|Value~' + note.value + '|Group~' + note.group + '|SubGroup~' + note.subgroup
              + '|Paymode~' + note.Paymode + '|Swipevalue~' + note.Swipevalue  + '}';
            });
            callback(result);
          }
          };

      _.each(receipt.get('lines').models, function (line) {
        var product = line.get('product');
        notes.push({
          searchkey: product.get('searchkey'),
          brandId: product.get('brand'),
          categoryId: product.get('productCategory'),
          value: line.get('discountedGross'),
          brand: null,
          department: null
        });
      });
      
      _.each(receipt.get('payments').models, function (payment) {
    	  if (OB.MobileApp.model.paymentnames[payment.get('kind')].paymentMethod.paymentMethodCategory$_identifier === 'Credit Card'){
    		  swipeValue+=payment.get('amount');
    		  payment.set('paymentMethodCategory$_identifier', 'Credit Card');
    		  
    		  if(!OB.UTIL.isNullOrUndefined(payment.get('sharccCardDetails'))){
    			  payModes.push(payment.get('sharccCardDetails').substring(0,6));
    		  }
    		  
    		  if(!OB.UTIL.isNullOrUndefined(payment.get('eftCardDetails'))){
    			  payModes.push(payment.get('eftCardDetails').substring(0,6)); 
    		  }
    	  }
      });
      
      _.each(notes, function (note) {
    	  note.Paymode = payModes.join(',');
    	  note.Swipevalue = swipeValue;
      });

      _.each(notes, function (note) {
        OB.Dal.find(OB.Model.Brand, {
          id: note.brandId
        }, function (brand) {
          if (brand.length === 1) {
            note.brand = brand.at(0).get('name');
          } else {
            note.brand = '-';
          }
          finish();
        });
        OB.CUSTSHA.findRootId({
          categoryId: note.categoryId,
          parentId: note.categoryId
        }).then(function (data) {
          note.department = data.category.get('searchKey');
          if (!OB.UTIL.isNullOrUndefined(data.Group)) {
            note.group = data.Group;
          }
          if (!OB.UTIL.isNullOrUndefined(data.Subgroup)) {
            note.subgroup = data.Subgroup;
          }
          finish();
        }, function (err) {
          note.department = '-';
          finish();
        });
      });
      finish();
    }

  };

}());