/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, $ */


(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_RenderOrderLine', function (args, callbacks) {
    var orderline = args.orderline;
    if (orderline.model.get('custqcIsgiftcard') || orderline.model.get('custqcIsreloadgiftcard')) {
      orderline.createComponent({
        style: 'display: block;',
        components: [{
          content: '** ' + orderline.model.get('custqcRealCardnumber') + ' **',
          attributes: {
            style: 'float: left; width: 100%;'
          }
        }, {
          style: 'clear: both;'
        }]
      });
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    return;
  });

}());