/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteLine', function (args, callbacks) {
    
    //verified returns
    var autoSettlePay = false,
        autoSettlePayments = [];
    if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('paymentData'))) {
      var paymentData = JSON.parse(localStorage.getItem('paymentData')).paymentData;

      args.order.get('lines').models.forEach(function (line) {
        if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
          var originalPayments = line.get('originalPayments');
          originalPayments.forEach(function (orgPayment) {
            if (!OB.UTIL.isNullOrUndefined(paymentData[orgPayment.kind]) && paymentData[orgPayment.kind]) {
              autoSettlePay = true;
              autoSettlePayments.push(orgPayment.name);
            }
          });
        }
      });

      args.order.get('payments').models.forEach(function (payment) {
        if (paymentData[payment.get('kind')]) {
          autoSettlePay = true;
        }
      });
    }
    
    //newly added code to identify it is bank bin receipt
/*    if(!OB.UTIL.isNullOrUndefined(args.order) && !OB.UTIL.isNullOrUndefined(args.order.get('isVerifiedReturn')) && args.order.get('isVerifiedReturn')){
    var isBankBinPromotionApplied = false;
    args.order.get('lines').models.forEach(function (line) {
        if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
          var originalPayments = line.get('originalPayments');
          originalPayments.forEach(function (orgPayment) {
            if (!OB.UTIL.isNullOrUndefined(orgPayment.paymentData) && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data) 
            && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data.custdisLinkedPaymentMethod)
             && orgPayment.paymentData.data.custdisLinkedPaymentMethod) {
               isBankBinPromotionApplied = true;
             } 
          });
        }
    });
  }*/

    if (args.order.get('isVerifiedReturn') && autoSettlePay && (args.selectedLines.length !== args.order.get('lines').length)) {
      let uniquePayments = [...new Set(autoSettlePayments)];
      args.cancelOperation = true;
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [uniquePayments] + " present in the Original Invoice -No partial Return Allowed");
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } else if (args.order.get('isVerifiedReturn') && autoSettlePay && (args.selectedLines.length === args.order.get('lines').length)) {
      //remove payment
      args.order.get('payments').models.forEach(function (payment) {
        if (paymentData[payment.get('kind')] && (payment.get('kind') != 'CUSTSPM_payment.giftcard_promo' && payment.get('kind') != 'CUSTSPM_payment.giftcard_store_promo')) {
          payment.set('automaticPGCPaymentRemoval', true, {
            silent: true
          });
          args.order.removePayment(payment);
        } else {
            args.cancelOperation = true;
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Line(s) can not be deleted since 'Auto Settle' payment method(s) added in the receipt. Please complete the transaction");
        }
      });
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } else {
/*      _.each(args.selectedLines, function (line) {
        if (line.get('custqcIsgiftcard') || line.get('custqcIsreloadgiftcard')) {
          if (!OB.MobileApp.model.get('connectedToERP')) {
            OB.info('Qwikcilver Model: id ' + OB.UTIL.get_UUID(),"cardNumber"+' '+line.get('custqcCardnumber'),"amount"+' '+(!OB.UTIL.isNullOrUndefined(line.get('custqcPurchaseamount')) ? line.get('custqcPurchaseamount') : line.get('custqcAmount')),"cardCurrencySymbol"+' '
            +line.get('custqcCardCurrencySymbol'),"currencyConversionRate"+' '+line.get('custqcCurrencyConversionRate'),"approvalCode"+' '+line.get('custqcApprovalcode'),"batchNumber"+' '+line.get('custqcBatchnumber'),"invoiceNumber"
            +' '+line.get('custqcInvoiceNumber'),"reference"+' '+line.get('custqcReferenceid'),"isGiftcard"+' '+line.get('custqcIsgiftcard'),"isReloadGiftcard"+' '+line.get('custqcIsreloadgiftcard'),"pos"+' '+OB.MobileApp.model.attributes.terminal.id);
            var query = "insert into CUSTQC_Qwikcilver(id, cardNumber, amount, cardCurrencySymbol, currencyConversionRate, approvalCode, batchNumber, invoiceNumber, reference, isGiftcard, isReloadGiftcard, pos)" 
                          + "values('" + OB.UTIL.get_UUID() + "',"
                          + "'" + line.get('custqcCardnumber') + "',"
                          + "'" + (!OB.UTIL.isNullOrUndefined(line.get('custqcPurchaseamount')) ? line.get('custqcPurchaseamount') : line.get('custqcAmount')) + "',"
                          + "'" + line.get('custqcCardCurrencySymbol') + "',"
                          + "'" + line.get('custqcCurrencyConversionRate') + "',"
                          + "'" + line.get('custqcApprovalcode') + "',"
                          + "'" + line.get('custqcBatchnumber') + "',"
                          + "'" + line.get('custqcInvoiceNumber') + "',"
                          + "'" + line.get('custqcReferenceid') + "',"
                          + "'" + line.get('custqcIsgiftcard') + "',"
                          + "'" + line.get('custqcIsreloadgiftcard') + "',"
                          + "'" + OB.MobileApp.model.attributes.terminal.id + "'"
                          +");";
            
            OB.Dal.queryUsingCache(OB.Model.CUSTQC_Qwikcilver, query, [], function (success) {
                line.set('custqcExecuteProcess', true);
                if (OB.CUSTQC.Utils.finishHook(args.selectedLines)) {
                    OB.UTIL.showLoading(false);
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
            });
          } else {
              OB.CUSTQC.Utils.removeLine(args.order, line, function (success) {
                if (success) {
                  if (OB.CUSTQC.Utils.finishHook(args.selectedLines)) {
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                  }
                } else {
                  args.cancelOperation = true;
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
              });
          }
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      });*/
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });
}());