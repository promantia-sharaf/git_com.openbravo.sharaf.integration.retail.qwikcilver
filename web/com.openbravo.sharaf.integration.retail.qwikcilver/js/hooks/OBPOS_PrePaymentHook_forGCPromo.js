(function () {
  OB.UTIL.QC = OB.UTIL.QC || {};
  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
    if (args.context.get('order').get('isVerifiedReturn')) {   
     var isBankBinPromotionApplied = false;
     var isNonBankBinPromotionApplied = false;
     _.each(args.context.get('order').get('lines').models, function(line) {
        if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
        line.get('originalPayments').forEach(function(payment){
          if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.data) && 
            !OB.UTIL.isNullOrUndefined(payment.paymentData.data.custdisLinkedPaymentMethod) && payment.paymentData.data.custdisLinkedPaymentMethod) {
                isBankBinPromotionApplied = true;
          } 
          if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && OB.UTIL.isNullOrUndefined(payment.paymentData.data)) {
                isNonBankBinPromotionApplied = true;
          } 
         })
        }
     }); 
      if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('paymentData'))) {
        var promoGCRedemptionAmt = 0,
            promoGCPresent = false;
        var paymentData = JSON.parse(localStorage.getItem('paymentData')).paymentData;
        if (OB.UTIL.isNullOrUndefined(args.context.get('order').get('origPaymentsAdded'))) {
          OB.MobileApp.model.get('payments').forEach(function (payment) {
            //check if autosettlement is true... if true add those payments
           if(!isBankBinPromotionApplied) {
            if (!OB.UTIL.isNullOrUndefined(paymentData[payment.paymentMethod.searchKey]) && paymentData[payment.paymentMethod.searchKey]) {
             OB.UTIL.QC.addAutoSettlePayments(payment.paymentMethod.searchKey, payment, args.context.get('order'));
            }
           }
           if(isBankBinPromotionApplied && isNonBankBinPromotionApplied) {
            if (!OB.UTIL.isNullOrUndefined(paymentData[payment.paymentMethod.searchKey]) && paymentData[payment.paymentMethod.searchKey]) {
             OB.UTIL.QC.addAutoSettlePayments(payment.paymentMethod.searchKey, payment, args.context.get('order'));
            }
           }           
          });
        }
      }
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });
}());

OB.UTIL.QC.addAutoSettlePayments = function (searchKey, paymentMethod, receipt) {

  //getOriginalPayments
  var originalPayments;
  receipt.get('lines').models.forEach(function (line) {
    if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
      originalPayments = line.get('originalPayments');
    }
  });

  //create payments if not added already
  if (!OB.UTIL.isNullOrUndefined(originalPayments)) {
    originalPayments.forEach(function (origPayment) {
      if (origPayment.kind === searchKey) {
        if (!OB.UTIL.isNullOrUndefined(origPayment.paymentData) && !OB.UTIL.isNullOrUndefined(origPayment.paymentData.QwikCilverTransaction) && 
         origPayment.paymentData.QwikCilverTransaction === 'DONE' && (origPayment.kind == 'CUSTSPM_payment.giftcard_promo' || origPayment.kind == 'CUSTSPM_payment.giftcard_store_promo')) {
          var process = new OB.DS.Process('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSCancelRedeem');
          process.exec({
            cardNumber: origPayment.paymentData.CardNumber,
            cardPin: origPayment.paymentData.CardPin,
            amount: origPayment.paymentData.RedeemAmount,
            cardCurrencySymbol: origPayment.paymentData.CardCurrencySymbol,
            currencyConversionRate: origPayment.paymentData.CurrencyConversionRate,
            invoiceNumber: origPayment.paymentData.InvoiceNumber,
            referenceId: origPayment.paymentData.ReferenceId,
            batchNumber: origPayment.paymentData.BatchId,
            approvalCode: origPayment.paymentData.ApprovalCode
          }, function (response) {
            if (response && !response.exception) {
              return true;
            } else {
               OB.CUSTQC.Utils.addGiftCardError(OB.MobileApp.model.receipt, 'qwikCilverPaymentErrors', {
                cardNumber: origPayment.paymentData.CardNumber,
                invoiceNumber: origPayment.paymentData.InvoiceNumber,
                approvalCode: origPayment.paymentData.ApprovalCode,
                amount: origPayment.paymentData.RedeemAmount,
                batchNumber: origPayment.paymentData.BatchId,
                referenceId: origPayment.paymentData.ReferenceId,
                notes: "Cancel Redeem"  + origPayment.paymentData.CardNumber + " with: " + origPayment.paymentData.RedeemAmount
              }, 'CANCEL_REDEEM');
            }
          });
        } 
        OB.UTIL.QC.addReceiptPayment(searchKey, paymentMethod, origPayment, receipt);
        receipt.set('origPaymentsAdded', true);
        receipt.save();
      }
    });
  }
};


OB.UTIL.QC.addReceiptPayment = function (searchKey, paymentMethod, origPayment, receipt) {

  var newPayment = new OB.Model.PaymentLine({
    kind: searchKey,
    name: paymentMethod.paymentMethod.name,
    amount: origPayment.amount,
    mulrate: paymentMethod.mulrate,
    isocode: paymentMethod.isocode,
    paymentData: {
      lineId: origPayment.paymentId
    }
  });
  receipt.addPayment(newPayment);
};