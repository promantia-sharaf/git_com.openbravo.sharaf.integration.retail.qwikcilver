(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_preRemovePayment', function (args, callbacks) {

    //for verified returns
	if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('paymentData'))) {
      var paymentData = JSON.parse(localStorage.getItem('paymentData')).paymentData,
          allowToDeletePayment = true,
          autoSettlePayments = '';
      if (args.receipt.get('isVerifiedReturn')) {
        if (paymentData[args.paymentToRem.get('kind')]) {
          allowToDeletePayment = false;
          autoSettlePayments = args.paymentToRem.get('name');
        }
        var storeGCpromoGC = false;
        args.payments.models.forEach(function (payment) {
          if (payment.get('kind') === 'CUSTSPM_payment.giftcard_promo' || payment.get('kind') === 'CUSTSPM_payment.giftcard_store_promo') {
            storeGCpromoGC = true;
          }
        });
       
         var restockingFeePM;
          if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('restockingFeePM')) && !OB.UTIL.isNullOrUndefined(localStorage.getItem('restockingFeePM').restockingFeePaymentMethod)) {
             restockingFeePM = JSON.parse(localStorage.getItem('restockingFeePM')).restockingFeePaymentMethod;
          }
       if(!OB.UTIL.isNullOrUndefined(restockingFeePM)) {    
            args.payments.models.forEach(function (payment) {
              if (!OB.UTIL.isNullOrUndefined(restockingFeePM.searchkey) && payment.get('kind') === restockingFeePM.searchkey) {
                       allowToDeletePayment = true;
              }
             });   
        }
        if (allowToDeletePayment === false && !args.paymentToRem.get('automaticPGCPaymentRemoval')) {
         if(storeGCpromoGC) { 
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [autoSettlePayments] + " cannot be deleted. Please complete the transaction");  
         } else{    
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [autoSettlePayments] + " cannot be manually deleted. It will be automatically deleted if all related promotion lines are removed");
         }
          args.cancellation = true;
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      } else {
        var paymentMethodCategory = args.paymentToRem.get('paymentMethodCategory$_identifier');
        var promoGCPresent = false,
            promoGCpayment = '';

        args.payments.models.forEach(function (payment) {
          if (payment.get('kind') === 'CUSTSPM_payment.giftcard_promo') {
            promoGCPresent = true;
            promoGCpayment = payment.get('name');
          }
        });

        if (paymentMethodCategory === 'Credit Card' && promoGCPresent) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [promoGCpayment] + " Present in the ticket, Please Remove");
          args.cancellation = true;
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      }
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());