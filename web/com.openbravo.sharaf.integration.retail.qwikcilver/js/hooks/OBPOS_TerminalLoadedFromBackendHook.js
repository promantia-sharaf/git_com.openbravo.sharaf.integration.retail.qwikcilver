OB.UTIL.HookManager.registerHook('OBPOS_TerminalLoadedFromBackend', function (args, callbacks) {
	
	new OB.DS.Process('com.openbravo.sharaf.integration.retail.qwikcilver.process.FetchAutoSettlementForReturn').exec({
		terminalType : OB.MobileApp.model.get('terminal').terminalType.id
	}, function(data) {
        if (data && data.exception) {
            if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
                console.log(data.exception.message);
            }
        } else {
        	localStorage.setItem('paymentData', JSON.stringify(data));
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
    });
});