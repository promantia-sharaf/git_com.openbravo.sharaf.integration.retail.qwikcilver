(function () {
  OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function (args, callbacks) {
     var isBankBinPromotionApplied = false;
     var linkedPaymentMethodName;
     _.each(args.order.receiptPayments, function(payment) {
       if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.data) && 
         !OB.UTIL.isNullOrUndefined(payment.paymentData.data.custdisLinkedPaymentMethod) && payment.paymentData.data.custdisLinkedPaymentMethod) {
           isBankBinPromotionApplied = true;
           linkedPaymentMethodName = payment.name;
         } 
     });
    if (args.selectedLines.length === 0) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    //Don't proceed further if All the lines are not selected(If original bill has PromoGC payment method)
    var promoGCPresent = false,
        autoSettlePayments = [];
    if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('paymentData'))) {
      var paymentData = JSON.parse(localStorage.getItem('paymentData')).paymentData;
      args.order.receiptPayments.forEach(function (payment) {
        if (paymentData[payment.kind]) {
          promoGCPresent = true;
          autoSettlePayments.push(payment.name);
        }
      });
    }

    //set originalPayments... so that it is available in prepayment hook
    if (promoGCPresent) {
      args.selectedLines.forEach(function (line) {
        line.line.originalPayments = args.order.receiptPayments;
      });
    }
   
    if (promoGCPresent && OB.MobileApp.model.receipt.attributes.lines.length > 0) {
      let uniquePayments = [...new Set(autoSettlePayments)];
      args.cancelOperation = true;
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "New products not allowed in Return documents with " + [uniquePayments]);
    } else {
      if (promoGCPresent && (args.selectedLines.length !== args.order.receiptLines.length) && (!isBankBinPromotionApplied && !linkedPaymentMethodName)) {
        let uniquePayments = [...new Set(autoSettlePayments)];
        args.cancellation = true;
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [uniquePayments] + " present in the Original Invoice -No partial Return Allowed");
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      } else {
        var paymentnotAvailable = false;
        var paymentName;
        var count = 0;        
        args.order.receiptPayments.forEach(function (payment) {
        if(payment.kind == 'CUSTSPM_payment.giftcard_promo') {
          if(OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames["CUSTSPM_payment.giftcard_promo"])) {
            paymentnotAvailable = true; 
            count = count + 1;
            paymentName = payment.name;
          } else {
            if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames["CUSTSPM_payment.giftcard_promo"].payment.cUSTSPMValidTill)) { 
                if(OB.MobileApp.model.paymentnames["CUSTSPM_payment.giftcard_promo"].payment.cUSTSPMValidTill < OB.UTIL.localStorage.getItem('businessdate')) {
                    paymentnotAvailable = true; 
                    count = count + 1;
                    paymentName = payment.name;
                 }
              }
            }
        }
        if(payment.kind == 'CUSTSPM_payment.giftcard_store_promo') {
          if(OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames["CUSTSPM_payment.giftcard_store_promo"])) {
            paymentnotAvailable = true;
            count = count + 1;
            paymentName = payment.name; 
          } else {
            if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames["CUSTSPM_payment.giftcard_store_promo"].payment.cUSTSPMValidTill)) {
                if(OB.MobileApp.model.paymentnames["CUSTSPM_payment.giftcard_store_promo"].payment.cUSTSPMValidTill < OB.UTIL.localStorage.getItem('businessdate')) {
                    paymentnotAvailable = true; 
                    count = count + 1;
                    paymentName = payment.name;
                 }   
              }
            }
        }      
       }); 
        if(promoGCPresent && paymentnotAvailable) {
          args.cancellation = true;
          let uniquePayments = [...new Set(autoSettlePayments)];
          if (count == 1) {
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Payment method (" + [paymentName] + ") used in the invoice is not configured in this terminal. Please contact PoS Support.");
          }
          if (count == 2) {
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Payment method (" + [uniquePayments] + ") used in the invoice is not configured in this terminal. Please contact PoS Support.");
          }          
         }        
       OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    }
  });
}());