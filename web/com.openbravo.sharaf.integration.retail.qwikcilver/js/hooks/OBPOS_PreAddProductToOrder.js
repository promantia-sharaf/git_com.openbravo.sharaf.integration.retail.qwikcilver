/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function (args, callbacks) {
    var me = this;
    var hasLineDiscountField = false;
    if (args.receipt.get('isQuotation') || args.cancelOperation) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    if (args.receipt.get('isVerifiedReturn') && !OB.UTIL.isNullOrUndefined(args.receipt) && !OB.UTIL.isNullOrUndefined(args.receipt.get('isVerifiedReturn'))){
    	 //newly added code to identify it is bank bin receipt
            var isBankBinPromotionApplied = false;
            args.receipt.get('lines').models.forEach(function (line) {
                if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
                  var originalPayments = line.get('originalPayments');
                  originalPayments.forEach(function (orgPayment) {
                    if (!OB.UTIL.isNullOrUndefined(orgPayment.paymentData) && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data) 
                    && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data.custdisLinkedPaymentMethod)
                     && orgPayment.paymentData.data.custdisLinkedPaymentMethod) {
                       isBankBinPromotionApplied = true;
                     } 
                  });
                }
            });
            
      if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('paymentData'))) {
        var autoSettlePaymentPresent = false;
        var autoSettlePayments = [];
        var paymentData = JSON.parse(localStorage.getItem('paymentData')).paymentData;
        var loyaltyPaymentPresent = false;
        var enabledLoyaltyPayMethod = new Array();
        if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')) && !OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod) {
           enabledLoyaltyPayMethod = JSON.parse(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod;
        }

        args.receipt.get('lines').models.forEach(function (line) {
          if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
            var originalPayments = line.get('originalPayments');
            originalPayments.forEach(function (orgPayment) {
              if (!OB.UTIL.isNullOrUndefined(paymentData[orgPayment.kind]) && paymentData[orgPayment.kind]) {
                autoSettlePaymentPresent = true;
                autoSettlePayments.push(orgPayment.name);
              }
            if (!OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[orgPayment.kind]) && enabledLoyaltyPayMethod[orgPayment.kind]) {
                loyaltyPaymentPresent = true;
             }
            });
          }
        });

        args.receipt.get('payments').forEach(function (payment) {
          if (!OB.UTIL.isNullOrUndefined(paymentData[payment.get('kind')]) && paymentData[payment.get('kind')]) {
            autoSettlePaymentPresent = true;
          }
        });
      
        if (autoSettlePaymentPresent) {
          let uniquePayments = [...new Set(autoSettlePayments)];
        //removing  restriction of adding articles in return receipt as part of OBDEV-265 ticket only for ban bin orders using isBankBinPromotionApplied flag
          if( !(uniquePayments.length === 1 && loyaltyPaymentPresent) && !isBankBinPromotionApplied) {
            args.cancelOperation = true;
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "New products not allowed in Return documents with " + [uniquePayments]);
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          }
        }
      }
    }

    if (args.productToAdd.get('custqcIsgiftcard') || args.productToAdd.get('custqcIsreloadgiftcard')) {
      OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
        popup: 'CUSTQC_ModalQwikCilverGiftCard',
        args: {
          isGiftcard: args.productToAdd.get('custqcIsgiftcard'),
          isReloadGiftcard: args.productToAdd.get('custqcIsreloadgiftcard'),
          isBalanceEnquiry: false,
          callback: function (success, model, data) {
            args.cancelOperation = !success;
            if (success) {
              var found = false;
              _.each(args.receipt.get('lines').models, function (line) {
                if (line.get('product').get('custqcIsgiftcard') || line.get('product').get('custqcIsreloadgiftcard')) {
                  if (line.get('custqcCardnumber') === model.custqcCardNumber || line.get('custqcBarcodenumber') === model.custqcCardNumber) {
                    found = true;
                  }
                }
              });
              if (found) {
                args.cancelOperation = true;
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTQC_LblQwikCilverCardAlreadyInOrder'));
              } else {
                args.attrs = args.attrs || {};
                args.attrs.custqcServerStatus = 'PENDING';
                args.attrs.custqcAmount = model.custqcAmount;
                args.attrs.custqcIsgiftcard = model.custqcIsgiftcard;
                args.attrs.custqcIsreloadgiftcard = model.custqcIsreloadgiftcard;
                args.attrs.custqcCardnumber = model.custqcCardNumber;
                args.attrs.custqcCardCurrencySymbol = data.cardCurrencySymbol;
                args.attrs.custqcCurrencyConversionRate = data.currencyConversionRate;
                args.attrs.custqcBatchNumber = data.batchNumber;
                args.attrs.custqcRealCardnumber = data.cardNumber;


                args.productToAdd.set('originalStandardPrice', model.custqcAmount);
                args.productToAdd.set('listPrice', model.custqcAmount);
                args.productToAdd.set('standardPrice', model.custqcAmount);
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              }
            } else {
              OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
          }
        }
      });
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());
