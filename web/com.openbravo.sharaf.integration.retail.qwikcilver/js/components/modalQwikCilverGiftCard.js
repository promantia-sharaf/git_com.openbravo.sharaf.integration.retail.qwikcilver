/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'CUSTQC.ModalQwikCilverGiftCard_btnApply',
  isDefaultAction: true,
  i18nContent: 'OBPOS_LblApplyButton',
  events: {
    onApplyChanges: ''
  },
  tap: function () {
    if (this.doApplyChanges()) {
      this.doHideThisPopup();
    }
  }
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'CUSTQC.ModalQwikCilverGiftCard_btnCancel',
  isDefaultAction: false,
  i18nContent: 'OBMOBC_LblCancel',
  events: {
    onCancelChanges: ''
  },
  tap: function () {
    this.doCancelChanges();
    this.doHideThisPopup();

  }
});

enyo.kind({
  kind: 'OB.UI.ModalAction',
  name: 'CUSTQC.ModalQwikCilverGiftCard',
  i18nHeader: 'CUSTQC_LblQwikCilverGiftCardCaption',
  topPosition: '60px',
  events: {
    onAddProduct: ''
  },
  handlers: {
    onApplyChanges: 'applyChanges',
    onCancelChanges: 'cancelChanges',
    onValidate: 'validate'
  },

  //body of the popup
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '375px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },

  //buttons of the popup
  bodyButtons: {
	  components: [{
		  style: 'clear: both; padding: 10px; color: red;',
		  name: 'errorIc'
	  },{
      kind: 'CUSTQC.ModalQwikCilverGiftCard_btnApply',
      name: 'btnApply'
    }, {
      kind: 'CUSTQC.ModalQwikCilverGiftCard_btnCancel'
		  }]
  },

  newAttributes: [{
    kind: 'OB.UI.renderTextProperty',
    name: 'custqcCardNumber',
    modelProperty: 'custqcCardNumber',
    i18nLabel: 'CUSTQC_LblQwikCilverGiftCardNumber',
    mandatory: true,
    isValid: function () {
      var val = this.getNodeProperty("value", this.value).trim();
      if (val.match(/^\d+$/) !== null) {
        val = parseInt(val, 10);
        return !isNaN(val);
      } else {
        return false;
      }
    }
  }, {
    kind: 'OB.UI.renderTextProperty',
    name: 'custqcAmount',
    modelProperty: 'custqcAmount',
    i18nLabel: 'CUSTQC_LblQwikCilverGiftCardAmount',
    mandatory: true,
    isValid: function () {
      var val = this.getNodeProperty("value", this.value).trim();
      if (val.match(/^\d*\.{0,1}\d*$/) !== null || val.match(/^\d*\,{0,1}\d*$/) !== null) {
        val = this.getFloatValue(val);
        return !isNaN(val);
      } else {
        return false;
      }
    },

    getFloatValue: function (value) {
      if (value) {
        value = value.replace(',', '.');
        return parseFloat(value);
      }
      return null;
    },
    getValue: function () {
      return this.getFloatValue(this.getNodeProperty("value", this.value));
    }
  }],

  applyChanges: function (inSender, inEvent) {
    var valid = this.validate();
    if (valid) {
	  var amount = this.model.custqcAmount;
	  var isminInValid = false;
	  var ismaxInValid = false;
	  var minLoadLimit = OB.MobileApp.model.attributes.permissions.CUSTQC_min_load_limit;
	  var maxLoadLimit = OB.MobileApp.model.attributes.permissions.CUSTQC_max_load_limit;
	  var minLimitCheck = OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.CUSTQC_min_load_limit);
	  var maxLimitCheck = OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.CUSTQC_max_load_limit);
	  if(!minLimitCheck){
		isminInValid=amount>=Number(minLoadLimit)?false:true;
		}
		if(!maxLimitCheck){
	    ismaxInValid =   amount<=Number(maxLoadLimit)?false:true;
	    }
      var cardNumberErr = this.model.custqcCardNumber.length !== 26 && this.model.custqcCardNumber.length !== 31;
      if (this.model.custqcisBalanceEnquiry && cardNumberErr) {
        cardNumberErr = this.model.custqcCardNumber.length !== 16;
      }
      if (cardNumberErr) {
        OB.UTIL.showError(OB.I18N.getLabel('CUSTQC_QwikCilverErrInvalidBarCode'));
        var property = OB.CUSTQC.Utils.getProperty(this.properties, 'custqcCardNumber');
        property.setValue('');
        property.focus();
        return false;
      }
		if (!minLimitCheck || !maxLimitCheck) {
			if ((isminInValid || ismaxInValid ) && !this.args.isBalanceEnquiry) {
				this.$.bodyButtons.$.errorIc.setContent(OB.I18N.getLabel('CUSTQC_AmountLimitErrMsg'));
				return;
			}
		}

      var me = this;
      var process = new OB.DS.Request('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSBalanceEnquiry');
      process.exec({
        cardNumber: this.model.custqcCardNumber,
        amount: this.model.custqcAmount,
        isGiftcard: this.model.custqcIsgiftcard,
        isReloadGiftcard: this.model.custqcIsreloadgiftcard,
        isBalanceEnquiry: this.model.custqcisBalanceEnquiry
      }, function (response) {
        if (response && !response.exception) {
          me.callback(true, me.model, response.data);
        } else {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), response.exception.message);
          me.callback(false);
        }
      }, function (err) {
        if (err && err.exception) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), err.exception.message);
          me.callback(false);
        }
      });
    }
    return valid;  
  },

  cancelChanges: function () {
    this.callback(false);
  },

  executeOnShow: function () {
    this.$.bodyButtons.$.errorIc.setContent('');
    this.callback = this.args.callback;
    this.model.custqcIsgiftcard = this.args.isGiftcard;
    this.model.custqcIsreloadgiftcard = this.isGiftcard ? false : this.args.isReloadGiftcard;
    this.model.custqcisBalanceEnquiry = this.args.isBalanceEnquiry;
    this.$.bodyContent.$.attributes.$.line_custqcAmount.setShowing(!this.args.isBalanceEnquiry);
    var property = OB.CUSTQC.Utils.getProperty(this.properties, 'custqcAmount');
    property.setValue('');
    property = OB.CUSTQC.Utils.getProperty(this.properties, 'custqcCardNumber');
    property.setValue('');
    window.setTimeout(function () {
      property.focus();
    }, 300);
  },

  validate: function () {
    var hasError = false;
    _.each(this.properties, function (comp) {
      if (comp.showing) {
        var property = comp.$.newAttribute.children[0];
        var value = property.getValue();
        if ((property.mandatory && !value) || (typeof (property.isValid) === "function" && !property.isValid())) {
          hasError = true;
          property.addClass('custqc-edit-error');
        } else {
          property.removeClass('custqc-edit-error');
        }
        this.model[property.modelProperty] = value;
      }
    }, this);
    return !hasError;
  },

  initComponents: function () {
    this.inherited(arguments);
    this.model = {};
    this.properties = [];
    enyo.forEach(this.newAttributes, function (natt) {
      this.properties.push(this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + natt.name,
        newAttribute: natt
      }));
    }, this);
  }

});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTQC.ModalQwikCilverGiftCard',
  name: 'CUSTQC_ModalQwikCilverGiftCard'
});