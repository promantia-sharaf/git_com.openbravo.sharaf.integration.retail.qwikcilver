/*
 ************************************************************************************
 * Copyright (C) 2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
OB.UI.ModalPayment.extend({
  executeOnShow: function () {
    if (this.args.receipt && this.args.paymentMethod && this.args.key && this.args.key === 'CUSTSPM_payment.giftcard' && !OB.UTIL.isNullOrUndefined(this.args.paymentMethod.overpaymentLimit) && this.args.paymentMethod.overpaymentLimit >= 0 && (OB.DEC.abs((this.args.receipt.getTotal()-this.args.receipt.get('payment'))+this.args.paymentMethod.overpaymentLimit) < this.args.amount)) {
      var symbol = OB.MobileApp.model.get('terminal').symbol;
      var symbolAtRight = OB.MobileApp.model.get('terminal').currencySymbolAtTheRight;
      var amount = OB.DEC.abs(this.args.receipt.getTotal()-(this.args.amount + this.args.receipt.get('payment')));
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_OverpaymentWarningTitle'), OB.I18N.getLabel('CUSTQC_OverpaymentWarningBody', [OB.I18N.formatCurrencyWithSymbol(amount, symbol, symbolAtRight)]));
      return false;
    } else {
      this.$.header.setContent(this.args.receipt && this.args.receipt.getTotal() > 0 ? OB.I18N.getLabel('OBPOS_LblModalPayment', [OB.I18N.formatCurrency(this.args.amount)]) : OB.I18N.getLabel('OBPOS_LblModalReturn', [OB.I18N.formatCurrency(this.args.amount)]));

      this.$.bodyContent.destroyComponents();
      //default values to reset changes done by a payment method
      this.closeOnEscKey = this.dfCloseOnEscKey;
      this.autoDismiss = this.dfAutoDismiss;
      this.executeOnShown = null;
      this.executeBeforeHide = null;
      this.executeOnHide = null;
      this.$.bodyContent.createComponent({
        mainPopup: this,
        kind: this.args.provider,
        paymentMethod: this.args.paymentMethod,
        paymentType: this.args.name,
        paymentAmount: this.args.amount,
        isocode: this.args.isocode,
        key: this.args.key,
        receipt: this.args.receipt,
        cashManagement: this.args.cashManagement,
        allowOpenDrawer: this.args.paymentMethod.allowopendrawer,
        isCash: this.args.paymentMethod.iscash,
        openDrawer: this.args.paymentMethod.openDrawer,
        printtwice: this.args.paymentMethod.printtwice,
        isReversePayment: this.args.isReversePayment,
        reversedPaymentId: this.args.reversedPaymentId,
        reversedPayment: this.args.reversedPayment
      }).render();
    }
  }
});