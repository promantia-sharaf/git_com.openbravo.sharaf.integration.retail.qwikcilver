/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

enyo.kind({
  name: 'CUSTQC_.UI.QwikCilverStorePromotionConnector',
  events: {
    onShowPopup: '',
    onHideThisPopup: ''
  },
  components: [{
    components: [{
      classes: 'row-fluid',
      components: [{
        classes: 'span4',
        content: 'Payment type'
      }, {
        name: 'paymenttype',
        classes: 'span8',
        style: 'font-weight: bold;'
      }]
    }, {
      classes: 'row-fluid',
      components: [{
        classes: 'span4',
        style: 'padding-top: 5px;',
        name: 'cardNumberLabel'
      }, {
        classes: 'span8',
        components: [{
          name: 'cardNumber',
          classes: 'input',
          style: 'width: 85%',
          kind: 'enyo.Input',
          type: 'text'
        }]
      }]
    }]
  }, {
    kind: 'OB.UI.ModalDialogButton',
    name: 'btnOk',
    style: 'float: right;',
    ontap: 'confirmPayment'
  }, {
    kind: 'OB.UI.ModalDialogButton',
    name: 'btnCancel',
    style: 'float: right;',
    content: 'OK',
    ontap: 'cancelPayment'
  }],

  voidTransaction: function (callback, receipt, removedPayment) {
    var paymentData = removedPayment.get('paymentData'),
        msg = OB.I18N.formatCurrency(removedPayment.get('amount'));
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTQC_QwikCilverPaymentRemove'), OB.I18N.getLabel('CUSTQC_QwikCilverPaymentAmount', [msg]), [{
      isConfirmButton: true,
      label: OB.I18N.getLabel('OBMOBC_LblOk'),
      action: function () {
        if (!OB.UTIL.isNullOrUndefined(paymentData) && !OB.UTIL.isNullOrUndefined(paymentData.QwikCilverTransaction) && paymentData.QwikCilverTransaction === 'DONE') {
          var process = new OB.DS.Process('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSCancelRedeem');
          process.exec({
            cardNumber: paymentData.CardNumber,
            cardPin: paymentData.CardPin,
            amount: paymentData.RedeemAmount,
            cardCurrencySymbol: paymentData.CardCurrencySymbol,
            currencyConversionRate: paymentData.CurrencyConversionRate,
            invoiceNumber: paymentData.InvoiceNumber,
            referenceId: paymentData.ReferenceId,
            batchNumber: paymentData.BatchId,
            approvalCode: paymentData.ApprovalCode
          }, function (response) {
            if (response && !response.exception) {
              callback(false);
            } else {
              OB.CUSTQC.Utils.addGiftCardError(OB.MobileApp.model.receipt, 'qwikCilverPaymentErrors', {
              cardNumber: paymentData.CardNumber,
              invoiceNumber: OB.MobileApp.model.receipt.get('documentNo'),
              approvalCode: '-',
              amount: paymentData.RedeemAmount,
              batchNumber: paymentData.BatchId,
              referenceId: 0,
              notes: "Cancel Redeem of " + amountRedeem + " for: " + cardNumber,
              errorLog: response.exception.message
              }, 'CANCEL_REDEEM');                 
              callback(true, response.exception.message);
            }
          }, function (err) {
            if (err && err.exception) {
              if (err.exception.status.timeout) {
                callback(true, 'Can not remove the payment in offline mode');
              } else {
              OB.CUSTQC.Utils.addGiftCardError(OB.MobileApp.model.receipt, 'qwikCilverPaymentErrors', {
              cardNumber: paymentData.CardNumber,
              invoiceNumber: OB.MobileApp.model.receipt.get('documentNo'),
              approvalCode: '-',
              amount: paymentData.RedeemAmount,
              batchNumber: paymentData.BatchId,
              referenceId: 0,
              notes: "Cancel Redeem of " + amountRedeem + " for: " + cardNumber,
              errorLog: err.exception.message,
              isPendingTxn: false
              }, 'CANCEL_REDEEM');                   
                callback(true, err.exception.message);
              }
            } else {
              callback(true, 'Can not remove the payment');
            }
          });
        } else if (!OB.UTIL.isNullOrUndefined(paymentData.CardNumber)) {
          OB.CUSTQC.Utils.addGiftCardError(receipt, 'qwikCilverPaymentErrors', {
            cardNumber: paymentData.CardNumber,
            invoiceNumber: paymentData.InvoiceNumber,
            approvalCode: paymentData.ApprovalCode,
            amount: paymentData.RedeemAmount,
            batchNumber: paymentData.BatchId,
            referenceId: paymentData.ReferenceId,
            notes: paymentData.Notes
          }, 'CANCEL_REDEEM');
          callback(false);
        } else {
          callback(false);
        }
      }
    }, {
      label: OB.I18N.getLabel('OBMOBC_LblCancel'),
      action: function () {
        callback(true, OB.I18N.getLabel('CUSTQC_QwikCilverPaymentCancelRemove'));
      }
    }]);
  },

  cancelPayment: function () {
    // Close the popup
    this.doHideThisPopup();
  },

  confirmPayment: function () {
    var valid = this.validate();
    if (valid) {
      var me = this,
          cardNumber = this.$.cardNumber.getValue(),
          posCurrency = OB.MobileApp.model.get('currency').iSOCode,
          processBalanceEnquiry = new OB.DS.Request('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSBalanceEnquiry');

      processBalanceEnquiry.exec({
        cardNumber: cardNumber,
        isGiftcard: false,
        isReloadGiftcard: false,
        isBalanceEnquiry: true
      }, function (response) {

        var insertPayment = function (paymentData, cardCurrencySymbol, currencyConversionRate, amountRedeem) {
            var newPayment = new OB.Model.PaymentLine({
              'kind': me.key,
              'name': me.paymentType,
              'paymentData': paymentData
            });
            if (posCurrency !== response.data.cardCurrencySymbol) {
              newPayment.set('amount', parseFloat(amountRedeem / currencyConversionRate));
              newPayment.set('origAmount', parseFloat(amountRedeem));
              newPayment.set('mulrate', currencyConversionRate);
              newPayment.set('isocode', cardCurrencySymbol);
            } else {
              newPayment.set('amount', parseFloat(amountRedeem));
            }

            me.receipt.addPayment(newPayment);
            };

        var makePayment = function (cardCurrencySymbol, currencyConversionRate, amountRedeem, paymentAmount, cardPin, cardExpiry, batchNumber) {

            OB.CUSTQC.Utils.getReceiptNotes(me.receipt, function (notes) {
              var processRedeem = new OB.DS.Process('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSRedeem');
              processRedeem.exec({
                cardNumber: cardNumber,
                cardPin: cardPin,
                invoiceNumber: OB.MobileApp.model.receipt.get('documentNo'),
                cardCurrencySymbol: cardCurrencySymbol,
                currencyConversionRate: currencyConversionRate,
                amount: amountRedeem,
                billAmount: paymentAmount,
                notes: notes,
                orderId: me.receipt.get('id')
              }, function (response) {
                if (response && !response.exception) {
                  var data = response.data,
                      paymentData = {
                      QwikCilverTransaction: data.qwikCilverTransaction,
                      CardNumber: data.cardNumber,
                      CardPin: cardPin,
                      CardExpiry: data.cardExpiry ? OB.I18N.formatDate(new Date(data.cardExpiry)) : null,
                      CardBalance: data.amount,
                      CardCurrencySymbol: data.cardCurrencySymbol,
                      CurrencyConversionRate: data.currencyConversionRate,
                      InvoiceNumber: data.invoiceNumber,
                      ApprovalCode: data.approvalCode,
                      RedeemAmount: data.redeemAmount,
                      BatchId: data.batchNumber,
                      ReferenceId: data.transactionId,
                      Notes: data.notes,
                      voidConfirmation: false,
                      voidTransaction: me.voidTransaction
                      };

                  insertPayment(paymentData, cardCurrencySymbol, currencyConversionRate, amountRedeem);
                } else {
                    OB.CUSTQC.Utils.addGiftCardError(OB.MobileApp.model.receipt, 'qwikCilverPaymentErrors', {
                      cardNumber: cardNumber,
                      invoiceNumber: OB.MobileApp.model.receipt.get('documentNo'),
                      approvalCode: '-',
                      amount: amountRedeem,
                      batchNumber: batchNumber,
                      referenceId: 0,
                      notes: "Redeem of " + amountRedeem + " for: " + cardNumber,
                      errorLog: response.exception.message,
                      isPendingTxn: false
                    }, 'REDEEM');                    
                  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), response.exception.message);
                }
              }, function (err) {
                if (err && err.exception) {
                  if (err.exception.status.timeout) {
                    var paymentData = {
                      QwikCilverTransaction: 'ERROR',
                      CardNumber: cardNumber,
                      CardPin: cardPin,
                      CardExpiry: cardExpiry,
                      CardBalance: amountRedeem,
                      CardCurrencySymbol: cardCurrencySymbol,
                      CurrencyConversionRate: currencyConversionRate,
                      InvoiceNumber: OB.MobileApp.model.receipt.get('documentNo'),
                      ApprovalCode: '-',
                      RedeemAmount: amountRedeem,
                      BatchId: batchNumber,
                      ReferenceId: 0,
                      Notes: "Redeem of " + amountRedeem + " for: " + cardNumber,
                      voidConfirmation: false,
                      voidTransaction: me.voidTransaction
                    };

                    OB.CUSTQC.Utils.addGiftCardError(OB.MobileApp.model.receipt, 'qwikCilverPaymentErrors', {
                      cardNumber: paymentData.CardNumber,
                      invoiceNumber: paymentData.InvoiceNumber,
                      approvalCode: paymentData.ApprovalCode,
                      amount: paymentData.RedeemAmount,
                      batchNumber: paymentData.BatchId,
                      referenceId: paymentData.ReferenceId,
                      notes: paymentData.Notes
                    }, 'REDEEM');
                    insertPayment(paymentData, cardCurrencySymbol, currencyConversionRate, amountRedeem);
                  } else {
                    OB.CUSTQC.Utils.addGiftCardError(OB.MobileApp.model.receipt, 'qwikCilverPaymentErrors', {
                      cardNumber: cardNumber,
                      invoiceNumber: OB.MobileApp.model.receipt.get('documentNo'),
                      approvalCode: '-',
                      amount: amountRedeem,
                      batchNumber: batchNumber,
                      referenceId: 0,
                      notes: "Redeem of " + amountRedeem + " for: " + cardNumber,
                      errorLog: err.exception.message,
                      isPendingTxn: false
                    }, 'REDEEM');                    
                    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), err.exception.message);
                  }
                }
              });
            });
            };

        // Close the popup
        me.doHideThisPopup();
        if (response && !response.exception) {
          var info = [],
              paymentAmount = posCurrency === response.data.cardCurrencySymbol ? me.paymentAmount : me.paymentAmount * response.data.currencyConversionRate,
              amountRedeem = paymentAmount < response.data.amount ? paymentAmount : response.data.amount;

          var processRedeem = new OB.DS.Request('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSCheckQwikCilverError');
          processRedeem.exec({
            cardNumber: response.data.cardNumber,

          }, function (result) {
            if (result && !result.exception) {
              if (!result["Card Error"]) {
                var storePromoGroupType = OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.storepromo_grouptype);
                var storePromoGCT = false;
                if(!storePromoGroupType && !OB.UTIL.isNullOrUndefined(response.data.CardProgramGroupType)) {
                  storePromoGCT =  OB.MobileApp.model.attributes.permissions.storepromo_grouptype.includes(response.data.CardProgramGroupType.trim());
                }
                if (!OB.UTIL.isNullOrUndefined(response.data.CardProgramGroupType) && storePromoGCT) {
                  info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardNumber') + ': ' + response.data.cardNumber);
                  info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverCardProgramGroupType') + ': ' + response.data.CardProgramGroupType);
                  info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardBalance') + ': ' + OB.I18N.formatCurrency(response.data.amount) + ' ' + response.data.cardCurrencySymbol);
                  info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardExpiryDate') + ': ' + OB.I18N.formatDate(new Date(response.data.cardExpiry)));
                  var redeemInfo = OB.I18N.formatCurrency(amountRedeem) + ' ' + response.data.cardCurrencySymbol;
                  if (posCurrency !== response.data.cardCurrencySymbol) {
                    redeemInfo += ' (' + OB.I18N.formatCurrency(amountRedeem / response.data.currencyConversionRate) + ' ' + posCurrency + ')';
                  }
                  info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardAmountRedeem') + ': ' + redeemInfo);
                  if (response.data.amount === 0) {
                    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTQC_QwikCilverPaymentTitle'), info, [{
                      label: OB.I18N.getLabel('OBMOBC_LblCancel')
                    }]);
                  } else {
                    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTQC_QwikCilverPaymentTitle'), info, [{
                      isConfirmButton: true,
                      label: OB.I18N.getLabel('OBMOBC_LblOk'),
                      action: function () {
                        if (cardNumber.length === 16) {
                          me.doShowPopup({
                            popup: 'CUSTQC_ModalQwikCilverGiftCardPin',
                            args: {
                              callback: function (success, cardPin) {
                                if (success) {
                                  var data = response.data;
                                  makePayment(data.cardCurrencySymbol, data.currencyConversionRate, amountRedeem, paymentAmount, cardPin, OB.I18N.formatDate(new Date(data.cardExpiry)), data.batchNumber);
                                }
                              }
                            }
                          });
                        } else {
                          var data = response.data;
                          makePayment(data.cardCurrencySymbol, data.currencyConversionRate, amountRedeem, paymentAmount, null, OB.I18N.formatDate(new Date(data.cardExpiry)), data.batchNumber);
                        }
                        return true;
                      }
                    }, {
                      label: OB.I18N.getLabel('OBMOBC_LblCancel')
                    }]);
                  }
                } else {
                  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'This is not a valid Store Promotional Gift Card number. Please try with other payment option');
                }
              } else {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTQC_QwikCilverPendingOperation'));
              }
            } else {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), result.exception);
            }
          });
        } else {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), response.exception.message);
        }
      }, function (err) {
        // Close the popup
        me.doHideThisPopup();
        if (err && err.exception) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), err.exception.message);
        }
      });
    }
  },

  validate: function () {
    this.$.cardNumber.setStyle("width: 85%");
    var valid = false,
        val = this.$.cardNumber.getValue().trim();
    if (val.match(/^\d+$/) !== null) {
      var value = parseInt(val, 10);
      valid = !isNaN(value);
    }
    if (valid) {
      if (val.length !== 16 && val.length !== 26 && val.length !== 31) {
        OB.UTIL.showError(OB.I18N.getLabel('CUSTQC_QwikCilverErrInvalidBarCode'));
        this.$.cardNumber.setValue('');
        this.$.cardNumber.focus();
        valid = false;
      }
    }
    if (!valid) {
      this.$.cardNumber.setStyle("border: 1px solid red; width: 85%");
    }
    return valid;
  },

  initComponents: function () {
    this.inherited(arguments);
    this.$.paymenttype.setContent(this.paymentType);
    this.$.btnOk.setContent(OB.I18N.getLabel('OBMOBC_LblOk'));
    this.$.btnCancel.setContent(OB.I18N.getLabel('OBMOBC_LblCancel'));
    this.$.cardNumberLabel.setContent(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardNumber'));
  }
});