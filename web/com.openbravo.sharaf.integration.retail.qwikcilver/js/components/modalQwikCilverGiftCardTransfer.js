/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

enyo.kind({
  kind: 'CUSTQC.ModalQwikCilverGiftCard',
  name: 'CUSTQC.ModalQwikCilverGiftCardTransfer',
  i18nHeader: 'CUSTQC_LblQwikCilverGiftCardTransferCaption',
  topPosition: '60px',
  handlers: {
    onApplyChanges: 'applyChanges'
  },

  newAttributes: [{
    kind: 'OB.UI.renderTextProperty',
    name: 'custqcOldCardNumber',
    modelProperty: 'custqcOldCardNumber',
    i18nLabel: 'CUSTQC_LblQwikCilverGiftOldCardNumber',
    mandatory: true,
    isValid: function () {
      var val = this.getNodeProperty("value", this.value).trim();
      if (val.match(/^\d+$/) !== null) {
        val = parseInt(val, 10);
        return !isNaN(val);
      } else {
        return false;
      }
    }
  }, {
    kind: 'OB.UI.renderTextProperty',
    name: 'custqcNewCardNumber',
    modelProperty: 'custqcNewCardNumber',
    i18nLabel: 'CUSTQC_LblQwikCilverNewGiftCardNumber',
    mandatory: true,
    isValid: function () {
      var val = this.getNodeProperty("value", this.value).trim();
      if (val.match(/^\d+$/) !== null) {
        val = parseInt(val, 10);
        return !isNaN(val);
      } else {
        return false;
      }
    }
  }],

  applyChanges: function (inSender, inEvent) {
    var property, valid = this.validate();
    if (valid) {
      if (this.model.custqcNewCardNumber === this.model.custqcOldCardNumber) {
        OB.UTIL.showError(OB.I18N.getLabel('CUSTQC_QwikCilverErrTransferSameCard'));
        return false;
      }

      if (this.model.custqcOldCardNumber.length !== 16 && this.model.custqcOldCardNumber.length !== 26 && this.model.custqcOldCardNumber.length !== 31) {
        OB.UTIL.showError(OB.I18N.getLabel('CUSTQC_QwikCilverErrInvalidBarCode'));
        property = OB.CUSTQC.Utils.getProperty(this.properties, 'custqcOldCardNumber');
        property.setValue('');
        property.focus();
        return false;
      }

      if (this.model.custqcNewCardNumber.length !== 26 && this.model.custqcNewCardNumber.length !== 31) {
        OB.UTIL.showError(OB.I18N.getLabel('CUSTQC_QwikCilverErrInvalidBarCode'));
        property = OB.CUSTQC.Utils.getProperty(this.properties, 'custqcNewCardNumber');
        property.setValue('');
        property.focus();
        return false;
      }
      var me = this;
      var process = new OB.DS.Process('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSBalanceTransfer');
      process.exec({
        oldCardNumber: this.model.custqcOldCardNumber,
        newCardNumber: this.model.custqcNewCardNumber
      }, function (response) {
        if (response && !response.exception) {
          me.callback(true, me.model, response.data);
        } else {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), response.exception.message);
          me.callback(false);
        }
      }, function (err) {
        if (err && err.exception) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), err.exception.message);
          me.callback(false);
        }
      });
    }
    return valid;
  },

  executeOnShow: function () {
    this.callback = this.args.callback;
    this.model.custqcIsgiftcard = this.args.isGiftcard;
    this.model.custqcIsreloadgiftcard = this.isGiftcard ? false : this.args.isReloadGiftcard;
    this.model.custqcisBalanceEnquiry = this.args.isBalanceEnquiry;
    var property = OB.CUSTQC.Utils.getProperty(this.properties, 'custqcNewCardNumber');
    property.setValue('');
    property = OB.CUSTQC.Utils.getProperty(this.properties, 'custqcOldCardNumber');
    property.setValue('');
    window.setTimeout(function () {
      property.focus();
    }, 300);
  }

});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTQC.ModalQwikCilverGiftCardTransfer',
  name: 'CUSTQC_ModalQwikCilverGiftCardTransfer'
});