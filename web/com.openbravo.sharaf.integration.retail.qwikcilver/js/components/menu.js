/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  enyo.kind({
    name: 'CUSTQC.UI.MenuQwikCilverInitialize',
    kind: 'OB.UI.MenuAction',
    i18nLabel: 'CUSTQC_LblQwikCilverInitialize',
    permission: 'CUSTQC_QwikCilver.initialize',
    tap: function () {
      this.inherited(arguments); // auto close the menu
      if (!OB.MobileApp.model.get('connectedToERP')) {
        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
        return;
      }
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTQC_QwikCilverInitializeTitle'), OB.I18N.getLabel('CUSTQC_QwikCilverInitializeMsg'), [{
        isConfirmButton: true,
        label: OB.I18N.getLabel('OBMOBC_LblOk'),
        action: function () {
          var process = new OB.DS.Process('com.openbravo.sharaf.integration.retail.qwikcilver.process.WSInitialize');
          process.exec({}, function (data) {
            if (data && !data.exception) {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblSuccess'), OB.I18N.getLabel('CUSTQC_QwikCilverInitializeOk'));
            } else {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.message);
            }
          }, function (err) {
            if (err && err.exception) {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), err.exception.message);
            }
          });
          return true;
        }
      }]);
    },
    init: function (model) {
      this.model = model;
      if (OB.MobileApp.model.hasPermission(this.permission, true)) {
        this.show();
      } else {
        this.hide();
      }
    }
  });

  enyo.kind({
    name: 'CUSTQC.UI.MenuQwikCilverBalanceEnquiry',
    kind: 'OB.UI.MenuAction',
    i18nLabel: 'CUSTQC_LblQwikCilverBalanceEnquiry',
    permission: 'CUSTQC_QwikCilver.balance_enquiry',
    events: {
      onShowPopup: ''
    },
    tap: function () {
      this.inherited(arguments); // auto close the menu
      if (!OB.MobileApp.model.get('connectedToERP')) {
        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
        return;
      }
      this.doShowPopup({
        popup: 'CUSTQC_ModalQwikCilverGiftCard',
        args: {
          isGiftcard: false,
          isReloadGiftcard: false,
          isBalanceEnquiry: true,
          callback: function (success, model, data) {
            if (success) {
              var info = [];
              info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardNumber') + ': ' + data.cardNumber);
              if (!OB.UTIL.isNullOrUndefined(data.CardProgramGroupType)) {
                info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverCardProgramGroupType') + ': ' + data.CardProgramGroupType);
              }
              info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardBalance') + ': ' + OB.I18N.formatCurrency(data.amount) + ' ' + data.cardCurrencySymbol);
              info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardExpiryDate') + ': ' + OB.I18N.formatDate(new Date(data.cardExpiry)));
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTQC_LblQwikCilverBalanceEnquiry'), info);
            }
          }
        }
      });
    },
    init: function (model) {
      this.model = model;
      if (OB.MobileApp.model.hasPermission(this.permission, true)) {
        this.show();
      } else {
        this.hide();
      }
    }
  });

  enyo.kind({
    name: 'CUSTQC.UI.MenuQwikCilverBalanceTransfer',
    kind: 'OB.UI.MenuAction',
    i18nLabel: 'CUSTQC_LblQwikCilverBalanceTransfer',
    permission: 'CUSTQC_QwikCilver.balance_transfer',
    events: {
      onShowPopup: ''
    },
    tap: function () {
      this.inherited(arguments); // auto close the menu
      if (!OB.MobileApp.model.get('connectedToERP')) {
        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
        return;
      }
      this.doShowPopup({
        popup: 'CUSTQC_ModalQwikCilverGiftCardTransfer',
        args: {
          callback: function (success, model, data) {
            if (success) {
              var info = [];
              info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardNumber') + ': ' + data.cardNumber);
              info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardBalance') + ': ' + OB.I18N.formatCurrency(data.amount) + ' ' + data.cardCurrencySymbol);
              info.push(OB.I18N.getLabel('CUSTQC_LblQwikCilverGiftCardExpiryDate') + ': ' + OB.I18N.formatDate(new Date(data.cardExpiry)));
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTQC_LblQwikCilverBalanceTransfer'), info);
            }
          }
        }
      });
    },
    init: function (model) {
      this.model = model;
      if (OB.MobileApp.model.hasPermission(this.permission, true)) {
        this.show();
      } else {
        this.hide();
      }
    }
  });

  // Register menu items:
  OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
    kind: 'CUSTQC.UI.MenuQwikCilverInitialize'
  });
  OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
    kind: 'CUSTQC.UI.MenuQwikCilverBalanceEnquiry'
  });
  OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
    kind: 'CUSTQC.UI.MenuQwikCilverBalanceTransfer'
  });

}());