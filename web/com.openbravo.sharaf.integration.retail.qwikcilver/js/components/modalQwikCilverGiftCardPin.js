/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

enyo.kind({
  kind: 'CUSTQC.ModalQwikCilverGiftCard',
  name: 'CUSTQC.ModalQwikCilverGiftCardPin',
  i18nHeader: 'CUSTQC_LblQwikCilverGiftCardCaption',
  topPosition: '60px',
  handlers: {
    onApplyChanges: 'applyChanges'
  },

  newAttributes: [{
    kind: 'OB.UI.renderTextProperty',
    name: 'custqcCardPin',
    modelProperty: 'custqcCardPin',
    i18nLabel: 'CUSTQC_LblQwikCilverGiftCardPin',
    mandatory: true,
    type: 'password',
    isValid: function () {
      var val = this.getNodeProperty("value", this.value).trim();
      if (val.match(/^\d+$/) !== null) {
        val = parseInt(val, 10);
        return !isNaN(val);
      } else {
        return false;
      }
    }
  }],

  applyChanges: function (inSender, inEvent) {
    var valid = this.validate();
    if (valid) {
      this.callback(true, this.model.custqcCardPin);
    }
    return valid;
  },

  executeOnShow: function () {
    this.callback = this.args.callback;
    var property = OB.CUSTQC.Utils.getProperty(this.properties, 'custqcCardPin');
    property.setValue('');
    window.setTimeout(function () {
      property.focus();
    }, 300);
  }

});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTQC.ModalQwikCilverGiftCardPin',
  name: 'CUSTQC_ModalQwikCilverGiftCardPin'
});